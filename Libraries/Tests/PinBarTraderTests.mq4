//+------------------------------------------------------------------+
//|                                                  PinBarTests.mq4 |
//|                        Copyright 2018, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2018, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include<MTMore/TestLib.mqh> 
#include<PinBarTrader.mqh> 

class PinBarTraderTests: public ITest {
   public:
   
      static void run() {
         Asserter *asserter = new Asserter(__FILE__);
         PinBarTrader* pinBarTrader = new PinBarTrader();
         pinBarTrader.useMock();
         
         // Define some Bars
         s_mock_bar pin_bar; 
         pin_bar.high = 1.00050;
         pin_bar.open = 1.00000;
         pin_bar.close = 0.99900;
         pin_bar.low = 0.9400;
         
         s_mock_bar mocked_bars[2];
         mocked_bars[1] = pin_bar;
         pinBarTrader.setMockedBars(mocked_bars);
         
         asserter.expect(pinBarTrader.isValidSignal(), __LINE__).toBeTrue();
         
         // The low is too close to the close, not a big wick. 
         // Should not be a pin bar. 
         s_mock_bar non_pin_bar; 
         non_pin_bar.high = 1.00050;
         non_pin_bar.open = 1.00000;
         non_pin_bar.close = 0.99900;
         non_pin_bar.low = 0.99850;
         
         mocked_bars[1] = non_pin_bar;
         pinBarTrader.setMockedBars(mocked_bars);
         
         asserter.expect(pinBarTrader.isValidSignal(), __LINE__).toBeFalse();
         
         // TODO ??
         delete pinBarTrader;
         delete asserter;
      } 
      
};