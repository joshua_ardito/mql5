//+------------------------------------------------------------------+
//|                                                        Tests.mq4 |
//|                        Copyright 2018, Joshua Ardito             |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, Joshua Ardito"
#property library
#property strict

#include <SheldonOrderManager.mqh>
#include <TestLib.mqh>

// random time used to mock the actual time, for testing. 
#define TIME_CURRENT 1000
  

CSheldonOrder* getDefaultOrder() {
   CSheldonOrder* order = new CSheldonOrder();
   order.OrderType(OP_BUY); 
   order.Signal(SIGNAL_PIN_BAR); 
   order.Period(PERIOD_D1); 
   order.RiskType(RISK_STABLE); 
   order.TrailPips(100); 
   order.StopLoss(0.9); 
   return order;
}


class OrderManagerMock: public CSheldonOrderManager {

   public: 
      CList* getAllOrders() {
         return AllOrders;
      }
      
   protected: 
      virtual datetime timeCurrent() {
         return TIME_CURRENT;
      }
      
      virtual int sendOrder(CSheldonOrder& order) {
         return AllOrders.Total();
      }
};

class OrderManagerTestsImpl : ITest {
   
   private: 
      Asserter* asserter; 
      OrderManagerMock *OrderManager;
      double SRLevelArray[];
      
   public: 
      OrderManagerTestsImpl() {
         OrderManager = new OrderManagerMock();
         OrderManager.setExampleParameters();
         asserter = new Asserter(__FILE__);
      }
      ~OrderManagerTestsImpl() {
         delete asserter;
         delete OrderManager;
      }
      
   void testOrderManagerGetTPs(
      ENUM_ORDER_TYPE type, 
      double entry,  
      double& expected[],
      int line_number,
   ) {
      CSheldonOrder* order = getDefaultOrder();
      double actual[];
      order.OrderType(type);
      order.OpenPrice(entry);
      
      OrderManager.getTPs(order, SRLevelArray, actual);
      asserter.expect(actual, line_number).toEqual(expected);
   }
   
   void testAllAcceptable() {
       
       // Make acceptable ratio sufficiently small such that it
      // doesn't filter out any SR levels
      int sr_targets[] = {1, 2};
      OrderManager.setAcceptableRatio(0);
      OrderManager.setSRTargets(sr_targets);
      
      double sr_levels[] = {1.012150, 1.012300, 1.012345};
      ArrayResize(SRLevelArray, ArraySize(sr_levels));
      ArrayCopy(SRLevelArray, sr_levels);
      // Test buy with no SR above should get no targets
      double expected1[] = {};
      testOrderManagerGetTPs(OP_BUY, 1.1, expected1, __LINE__);
   
      
      // Sell with no SR below should get no targets
      double expected2[] = {};
      testOrderManagerGetTPs(OP_SELL, 0.9, expected2, __LINE__);
      
      // Buy with 1 SR above should get 1 target
      double expected3[] = {1.012345};
      testOrderManagerGetTPs(OP_BUY, 1.012330, expected3, __LINE__); 
      
      // Sell with 2 SR below should get 2 targets 
      double expected4[] = {1.012150, 1.012300};
      testOrderManagerGetTPs(OP_SELL, 1.012330, expected4, __LINE__);
      
      // Sell with 3 SR below should still only get 2 targets
      // since we only asked for 2
      double expected5[] = {};
      testOrderManagerGetTPs(OP_SELL, 0.9, expected5, __LINE__);
   }
   
   
   void testNotAllAcceptable() {
      // Filter out any orders less than one to one
      OrderManager.setAcceptableRatio(1);
      double sr_levels[] = {0.8, 0.9, 1.1, 1.3};
      ArrayResize(SRLevelArray, ArraySize(sr_levels));
      ArrayCopy(SRLevelArray, sr_levels);
      
      // Buy with 3 SR above should only set 1,
      // Since the other one has a bad ratio. 
      CSheldonOrder* order = getDefaultOrder();
      double actual[];
      double expected[] = {0.9};
      order.OrderType(OP_BUY);
      order.OpenPrice(0.7);
      order.StopLoss(0.59);
      order.Slippage(3); 
      order.TakeProfit(1);
      order.Lots(29);
      order.AssertValid();
      OrderManager.getTPs(order, SRLevelArray, actual);
      asserter.expect(actual, __LINE__).toEqual(expected);
   }
   
   void testOrderSend() {

      // Initialize some defaults
      OrderManager.setAcceptableRatio(0);// Ignore order filtering
      
      double exponential_risk = 15;
      OrderManager.setExponentialRisk(exponential_risk);
      
      double sr_levels[] = {0.8, 0.9, 1.1, 1.3};
      ArrayResize(SRLevelArray, ArraySize(sr_levels));
      ArrayCopy(SRLevelArray, sr_levels);
      
      CSheldonOrder* base_order;
      base_order.StopLoss(0.82);
      base_order.OpenPrice(0.85); 
      base_order.RiskType(RISK_EXPONENTIAL);
      base_order.Period(PERIOD_D1); 
      base_order.OrderType(OP_BUY); 
      base_order.Signal(SIGNAL_IB);
      base_order.TrailPips(5);
      base_order.Slippage(3); 
      base_order.TakeProfit(1);
      base_order.SetPercentageRisk(exponential_risk);
      base_order.AssertValid();
      
      double fib_level_order = 0.88;
      
      OrderManager.sendOrders(base_order, SRLevelArray, fib_level_order, 24);
      CList* all_orders = OrderManager.getAllOrders();
      
      // send 2 sr orders plus the fib order
      int expected_size = 3;
      
      asserter.expect(all_orders.Total(), __LINE__).toEqual(expected_size);
      
      CSheldonOrder* order1 = all_orders.GetNodeAtIndex(0);
      
      asserter.expect( (double)order1.Expiration(), __LINE__)
         .toEqual(TIME_CURRENT + (24 * 60 * 60));
         
      /* 
      asserter.expect(order1.RiskPercent(),__LINE__)
         .toEqual(exponential_risk / expected_size);
      */
      //Pull tps out of orders and sort
      double actual_tps[];
      for(CSheldonOrder* order = all_orders.GetFirstNode(); order != NULL; order = all_orders.GetNextNode()) {
         ArrayAppend(actual_tps, order.TakeProfit());
      }
      
      double expected_tps[] = {0.88, 0.9, 1.1};
      // TODO: Something is missing here ?? 
   }
   
   void run() {
      
      testAllAcceptable();
      testNotAllAcceptable();
      testOrderSend();
      delete (&this);
   }
   
};

class OrderManagerTests {
   public: 
      static void run() {
         new OrderManagerTestsImpl().run();
      }
};
