//+------------------------------------------------------------------+
//|                                                 TestLibTests.mq4 |
//|                        Copyright 2018, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2018, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <TestLib.mqh> 

class TestLibTests {
   public: 
      static void run() {
         
         Asserter* asserter = new Asserter(__FILE__);
         // Test IntExpecter
         asserter.expect(1, __LINE__).toEqual(1);
         //asserter.expect((bool)True, __LINE__).toBeFalse();
         
         // Test BoolExpecter
         bool isTrue = True;
         asserter.expect(isTrue, __LINE__).toBeTrue();
         
         // Test DoubleExpecter 
         double x = 1.2345;
         asserter.expect(x, __LINE__).toEqual(1.2345);
   
        // Test DoubleArrayExpecter         
         double expectedDouble[] = {1,2,3};
         double actualDouble[] = {2, 3, 1};
         asserter.expect(expectedDouble, __LINE__).toEqual(actualDouble);
         
         delete asserter;
      }
};

