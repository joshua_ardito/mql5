//+------------------------------------------------------------------+
//|                                                  PinBarTests.mq4 |
//|                        Copyright 2018, Joshua Ardito             |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, Joshua Ardito"
#property library      
#property strict

#include <IBTrader.mqh> 
#include <TestLib.mqh> 
#include <SRLevels.mqh> 
#include "OrderManagerTests.mq4"

class IBTraderTests: public ITest {
   public: 
      static void run() {
         
         Asserter* asserter = new Asserter(__FILE__);
         // Define some Bars
         s_mock_bar high_momentum_bar; 
         high_momentum_bar.high = 0.95100;
         high_momentum_bar.open = 0.94900;
         high_momentum_bar.close = 0.95200;
         high_momentum_bar.low = 0.9400;
         
         
         s_mock_bar parent_bar;
         parent_bar.high = 0.95234;
         parent_bar.open = high_momentum_bar.close;
         parent_bar.close = 0.95150;
         parent_bar.low = 0.95100;
         
         s_mock_bar inside_bar; 
         inside_bar.high = 0.95200; 
         inside_bar.open = parent_bar.close; 
         inside_bar.close = 0.95160;
         inside_bar.low = 0.95140;
         
         
         // Order the bars in an array
         s_mock_bar mocked_bars[4];
         mocked_bars[1] = parent_bar;
         mocked_bars[2] = inside_bar;
         mocked_bars[3] = high_momentum_bar;
         
         // Define somr SR levels
         double daily_sr[] = {0.94850, 0.95400,0.95500};
         double weekly_sr[] = {0.94700, 0.95600};
         
         // Define the trader mock, load some defaults, 
         IBTrader *ibTrader = new IBTrader();
         ibTrader.useMock();
         
         ibTrader.setExampleParameters();
         ibTrader.setMockedBars(mocked_bars);
         OrderManagerMock *orderManager = new OrderManagerMock();
         SRLevelsMock *srLevels = new SRLevelsMock();
         srLevels.setSRLevelValues(daily_sr);
         
         CChartContext *chartContext = new CChartContext();
         chartContext.setDailySRLevels(srLevels);
         
         ibTrader
            .setOrderManager(orderManager)
            .setChartContext(chartContext);
         
         // Set some specifics
         orderManager.setAcceptableRatio(0);
         int sr_targets[] = {1,2};
         orderManager.setSRTargets(sr_targets);
         
         // asserter.expect the IB signal to be invalid
         asserter.expect(ibTrader.isValidSignal(), __LINE__).toBeFalse();
         CSheldonOrder* new_order1 = new CSheldonOrder();
         
         // Test that no trade has been set since there is no signal
         asserter.expect((int)ibTrader.execute(new_order1), __LINE__)
            .toEqual(NO_TRADE_REASON_NO_SIGNAL);
         delete new_order1;
         
         //Swap the bars
         mocked_bars[1] = inside_bar; 
         mocked_bars[2] = parent_bar;
         ibTrader.setMockedBars(mocked_bars);
         
         // asserter.expect the IB signal to be valid
         asserter.expect(ibTrader.isValidSignal(), __LINE__).toBeTrue();
        
         // Test is exponential
         // 250 should be the delta, so at a 200 threshold its exponential
         ibTrader.setTrendThreshold(200);
         ibTrader.setSlowdownLookback(2);
         asserter.expect(ibTrader.isExponential(), __LINE__).toBeTrue();
         
         // and at a 300 threshold it is not
         ibTrader.setTrendThreshold(300);
         asserter.expect(ibTrader.isExponential(), __LINE__).toBeFalse();
         
         CSheldonOrder* new_order2 = new CSheldonOrder();
         
         // Check to make sure we do a buy as expected
         s_signal_info signal_info;
         ibTrader.getSignalInfo(signal_info);
         asserter.expect((int)signal_info.direction, __LINE__).toEqual(DIRECTION_BUY);
         
         // expect a trade to be made
         asserter.expect((int)ibTrader.execute(new_order2), __LINE__)
            .toEqual(NO_TRADE_REASON_NONE);
         delete new_order2;
         // We should be setting 3 trades. One for fib, and 2 for the set SRs. 
         int total = orderManager.getAllOrders().Total();
         asserter.expect(total, __LINE__).toEqual(3);
         
         // TODO: test for MA
         // TODO: test for trailing stop
         // TODO: change execute to setTradesIfSignalExists()
         // 
         
         
         delete ibTrader;
         delete orderManager;
         delete asserter;
         delete chartContext;
         delete srLevels;
      }     
};
