//+------------------------------------------------------------------+
//|                                                      CTrader.mqh |
//|                                    Copyright 2017, Joshua Ardito |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Joshua Ardito"
#property link      "https://www.mql5.com"
#property strict

#include<MTMore/OrderManager.mqh>
#include<MTMore/Utils.mqh>
#include<ChartContext.mqh>
#include<SheldonOrderManager.mqh>
#include<MockableChartDataAccessor.mqh>

struct s_signal_info {
   ENUM_DIRECTION direction;
   string display_message;
};

enum ENUM_NO_TRADE_REASON {
   // there is no reason not to trade, in fact we should trade
   NO_TRADE_REASON_NONE = 1,
   
   NO_TRADE_REASON_UNKNOWN = 2,
   // the trader returned false for isValidSignal
   NO_TRADE_REASON_NO_SIGNAL = 3,
   
   // the signal was valid but didnt meet the criteria of the trader 
   NO_TRADE_REASON_FAILED_CRITERIA = 4,
   
   // the governing trade parameters were not compatible with the
   // trade
   NO_TRADE_REASON_GOVERNING_TRADE_FAILED_VERIFY = 5,
   
   // this trade is meant to be traded with only stable risk
   // but we are ignoring stable trades
   NO_TRADE_REASON_IGNORING_STABLE = 6
   
};

class CTrader: public CMockableChartDataAccessor {
   protected:
   
      // gatekeepers 
      bool CheckTrend; 
      
      // level params
      double TrailPercent;
      // open or close trade params
      double EntryPercent;
      double TakeProfitPercent;
      double SLBufferPercent;
      
      // slowdown
      int SlowdownLookback;
      int ExpirationHrs;
      
      CSheldonOrderManager *OrderManager;
      CChartContext *ChartContext;
      
      // internal params
     string SignalMessage; 
     ENUM_OBJECT SignalObject;
   
   private: 
      ENUM_NO_TRADE_REASON executeImpl(
         CSheldonOrder*new_order,
         CSheldonOrder*governing_order
      );
      
      // ENUM_TREND getTrend();
      
   public:
      // Destructor
      ~CTrader(){
         delete OrderManager;
      }
      
      //+---------------VIRTUAL FUNCTIONS-------------------------------------+
      virtual bool isExponential() = NULL;
      virtual bool isValidSignal() = NULL;
      virtual void getSignalInfo(
         s_signal_info& signal_info
      ) = NULL;
      virtual string getSignalStringName() = NULL;
      virtual ENUM_SIGNAL_TYPE getSignalType() = NULL;
      
      virtual void setSpecificExampleParameters() = NULL;
      
      //+-------------------REGULAR FUNCTIONS --------------------------------+
      ENUM_NO_TRADE_REASON executeWithGoverningOrder(
         CSheldonOrder*new_order,
         CSheldonOrder*governing_order
      );
      
      ENUM_NO_TRADE_REASON CTrader::execute(
         CSheldonOrder* new_order
      );
      
      /*  Assumes that it is given a fully populated base order 
      * (everything but tp), and it will send the order off to the 
      * orderManager to be split up based on different sr targets
      */ 
      void CTrader::sendOrders(CSheldonOrder* new_order);
      
      /* Sets the parameters for the order to be sent to the order sender
       * (parameters such as entry, stop loss, risk, etc
       */ 
       
      void CTrader::populateOrder(
         s_signal_info& signal_info, 
         CSheldonOrder*new_order
      );

      /* Given the signal info which includes the direction of the signal and 
       * the message associated with that signal, draw the signal and message 
       * on the chart */ 
      void drawSignal(s_signal_info& signal_info);
      
      
      //+-------------------------SETTERS-------------------------------------+
      // FIXME: cant make the setters return pointer CTrader* because then we 
      // lose access to functions defined on inherited traders.
      // For example: (new IBTrader).setTrailPercent(10).IBSpecificMethod(); 
      // will not work since that IBSpecificMethod is defined on IBTrader 
      // and the setTrailPercent method returns a pointer to t
      // the generic CTrader. 
      // +--------------------------------------------------------------------+
      
      void setCheckTrend(bool check_trend) {
         CheckTrend = check_trend;
      }
      
      void setTrailPercent(double trail_percent) {
         TrailPercent = trail_percent;
      } 

      void setPeriod(ENUM_TIMEFRAMES period) {
         m_period = period;
      }
      
      void setSlowdownLookback(int slowdown_lookback) {
         SlowdownLookback = slowdown_lookback; 
      }
      
      CTrader* setOrderManager(COrderManager* order_manager) {
         delete OrderManager;
         OrderManager = order_manager;
         return (&this);
      }
      
      CTrader* setChartContext(CChartContext* chart_context) {
         delete ChartContext;
         ChartContext = chart_context;
         return (&this);
      }
      
      void setExpirationHrs(int expiration_hrs) {
         ExpirationHrs = expiration_hrs;
      }
      
      void setEntryPercent(double entry_percent ) {
         EntryPercent = entry_percent;
      }
      
      void setSLBufferPercent(double sl_buffer_percent) {
         SLBufferPercent = sl_buffer_percent;
      }
      
      void setTakeProfitPercent(double take_profit_percent) {
         TakeProfitPercent = take_profit_percent;
      }
      
      virtual void setExampleParameters() {
         TrailPercent = 110; 
         EntryPercent = 110;
         TakeProfitPercent = 161; 
         SLBufferPercent = 15;
         SlowdownLookback = 2;
         ExpirationHrs = 24;
         CSheldonOrderManager *order_manager = new COrderManager(); 
         order_manager.setExampleParameters();
         OrderManager = order_manager;
         setSpecificExampleParameters();
         
      };
      //+------------------------- SETTERS END ---------------------------------+
            
      ENUM_OBJECT getDisplayObject(ENUM_DIRECTION direction) {
         switch (direction) {
            case DIRECTION_NONE: 
               return OBJ_ARROW_STOP; 
            case DIRECTION_BUY: 
               return OBJ_ARROW_UP; 
            case DIRECTION_SELL: 
               return OBJ_ARROW_DOWN; 
            case DIRECTION_ANY: 
               return OBJ_ARROW;
            default: 
               Utils::exception("Got unexpected value for direction in CTrader::getDisplayObject");
               return 0;
         }
      }
      
      
      /* The reason we are not taking the trade, if any. 
       * If we SHOULD take the trade, this will return the value
       * NO_TRADE_REASON_NONE. 
       * It should also populate the signal_info struct with
       * information about what direction, if any, that we should
       * take the trade in. 
       */ 
      ENUM_NO_TRADE_REASON getNoTradeReason(
         s_signal_info& signal_info
      ) {
      
         ENUM_NO_TRADE_REASON reason = NO_TRADE_REASON_NONE;
         
         if (!isValidSignal()) {
            signal_info.direction = DIRECTION_NONE;
            return NO_TRADE_REASON_NO_SIGNAL;
         }
         
         getSignalInfo(signal_info);
                  
         switch (signal_info.direction) {
            case DIRECTION_ANY:
               Utils::exception("Both directions not implemented", __FILE__, __LINE__);
               reason = NO_TRADE_REASON_UNKNOWN; 
               break;
            case DIRECTION_BUY: 
            case DIRECTION_SELL: 
               reason = NO_TRADE_REASON_NONE;
               break;
            // signal is valid but previous function told us
            // why it failed
            case DIRECTION_NONE: 
               reason = NO_TRADE_REASON_FAILED_CRITERIA; 
               break;
         }
         
         return reason;
      }
      
      
      double getFibTakeProfit(ENUM_ORDER_TYPE order_type) {
         double size = high() - low();
         double tp_dist = size * (TakeProfitPercent / 100);
         return nd(lossBoundary() + (tp_dist * profitDirection()));
      }
      
};

ENUM_NO_TRADE_REASON CTrader::executeWithGoverningOrder(
   CSheldonOrder* new_order,
   CSheldonOrder* governing_order
) {
   if(governing_order == NULL) {
      Utils::exception(
         StringFormat("Second argument passed to %s was expected to be non-null", __FUNCTION__), 
         __FILE__, 
         __LINE__
      );
   }
   return executeImpl(new_order, governing_order);
   
}

ENUM_NO_TRADE_REASON CTrader::execute(
   CSheldonOrder* new_order
) {
   return executeImpl(new_order, NULL);
}
      
ENUM_NO_TRADE_REASON CTrader::executeImpl(
   CSheldonOrder* new_order,
   CSheldonOrder* governing_order
) {
   s_signal_info signal_info;
   
   ENUM_NO_TRADE_REASON no_trade_reason = 
      getNoTradeReason(signal_info);
   
   // didn't find a signal so don't draw or go any further
   if (no_trade_reason == NO_TRADE_REASON_NO_SIGNAL) {
      signal_info.direction = DIRECTION_NONE;
      return no_trade_reason;
   }
   
   // if we found a signal but still decided not to take the trade, 
   // draw the signal explaining why, but don't go any further
   if (signal_info.direction == DIRECTION_NONE) {
      drawSignal(signal_info);
      return no_trade_reason;
   }
   
   populateOrder( 
      signal_info,
      new_order
   );
      
   if (governing_order != NULL) {
      // check if direction matches daily
      if (
         (signal_info.direction == DIRECTION_BUY && governing_order.OrderType_() != OP_BUY) || 
         (signal_info.direction == DIRECTION_SELL && governing_order.OrderType_() != OP_SELL)
      ) {
         signal_info.display_message = "Direction did not match daily";
         no_trade_reason = NO_TRADE_REASON_GOVERNING_TRADE_FAILED_VERIFY;
      // check if h4 break out is below daily
      } else if (inProfitDirection(new_order.OpenPrice(), governing_order.OpenPrice())) {
         signal_info.display_message = "H4 entry above daily entry";
         no_trade_reason = NO_TRADE_REASON_GOVERNING_TRADE_FAILED_VERIFY;
      } 
   }
   
   // If we still have no reason not to take the trade, take it. 
   if (no_trade_reason == NO_TRADE_REASON_NONE){
      sendOrders(new_order);
   }
           
   drawSignal(signal_info);
   return no_trade_reason;
 }

void CTrader::populateOrder(
   s_signal_info& signal_info, 
   CSheldonOrder*new_order
) {

   if (signal_info.direction == DIRECTION_BUY) {
      new_order.OrderType_(OP_BUY); 
   } else if (signal_info.direction == DIRECTION_SELL) {
      new_order.OrderType_(OP_SELL);
   } else {
      string msg = 
         "expected either buy or sell when creating orders, got" +
         EnumToString(signal_info.direction);
      Utils::exception(msg, __FILE__,__LINE__);
   }
   
   setDirection(new_order.OrderType_());
   
   double size = high() - low();
   double sl_buffer = size * (SLBufferPercent / 100);
   double entry_percent = size * (EntryPercent / 100);
   new_order.RiskType(RISK_STABLE);
   new_order.Period(m_period);
   new_order.Signal(getSignalType());
   new_order.StopLoss(nd(lossBoundary() + (sl_buffer * lossDirection())));
   new_order.OpenPrice(nd(lossBoundary() + (profitDirection() * entry_percent)));
   new_order.RiskType(RISK_STABLE); 
   ENUM_TREND_DIRECTION trend_direction = ChartContext.getTrendDirection();
   
   bool properSell = trend_direction == TREND_DIRECTION_BEARISH && new_order.OrderType_() == OP_SELL;
   bool properBuy = trend_direction == TREND_DIRECTION_BULLISH && new_order.OrderType_() == OP_BUY;
   if (isExponential() && (!CheckTrend || properBuy || properSell)) {
         new_order.RiskType(RISK_EXPONENTIAL);
   }
}

void CTrader::sendOrders(CSheldonOrder* new_order) {
   
   double daily_sr[];
   double weekly_sr[];
   
   ChartContext.getDailySRLevel().getSRLevelValues(daily_sr);
   ChartContext.getWeeklySRLevel().getSRLevelValues(weekly_sr);
   
   OrderManager.sendOrders(
      new_order, 
      daily_sr, 
      weekly_sr,
      getFibTakeProfit(new_order.OrderType_()), 
      ExpirationHrs
   );
};

void CTrader::drawSignal(s_signal_info& signal_info) {

   double pos = low(1) - (200 * point());
   datetime time = time(1);
   
   string msg = signal_info.display_message;
   
   // Sanity check. If we didn't trade the signal, we should provide 
   // a message explaining why not. 
   if (
      signal_info.direction == DIRECTION_NONE &&
      ( msg == NULL || msg == "" )
   ) {
     Print("No reason provided as to why signal was not traded");
     Utils::exception(
      "No reason provided as to why signal wasn't traded",
       __FILE__ ,
       __LINE__
     );
   }
   
   string full_message = 
      TimeToStr(time) + "" +
      TimeframeToString(m_period) +
      " " +
      getSignalStringName() + 
      " " +
      msg;
   
   int res = ObjectCreate(
      full_message,
      getDisplayObject(signal_info.direction), 
      0, 
      time, 
      pos, 
      time + 50, 
      pos + 50
   );
   if (res == true) {
      Alert("Signal drawn at time ", time, "with name ", full_message);
   } else {
      Alert("Error creating object");
   }
}

// Mock Trader for use in tests. When testing don't draw signals
class CMockableTrader : public CTrader {
   void drawSignal(s_signal_info& signal_info) {
      if (Mocking) {
         return;
      } else {
         CTrader::drawSignal(signal_info);
      }
   } 
};