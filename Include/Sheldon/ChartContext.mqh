//+------------------------------------------------------------------+
//|                                                CChartContext.mqh |
//|                                   Copyright 2018, Joshua Ardito  |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, Joshua Ardito"
#property link      "https://www.mql5.com"
#property strict

#include<MTMore/SRLevels.mqh>
#include<MTMore/Order.mqh> 
#include <Indicators/Trend.mqh>
#include <ChartObjects/ChartObjectsTxtControls.mqh>
#include <Controls/Button.mqh>

#define TREND_OBJECT_NAME "TrendDirection"
#define TREND_DELTA_NAME "TrendDelta" 
#define TREND_BUTTON_NAME "TrendButton" 
#define FLOW_OBJECT_NAME "Flow"

enum ENUM_TREND_DIRECTION {
   TREND_DIRECTION_BEARISH = 1, 
   TREND_DIRECTION_BULLISH = 2, 
   TREND_DIRECTION_SIDEWAYS = 3
};

enum ENUM_WINGDING {
   WINGDING_STRAIGHT_FACE = 75, 
   WINGDING_UP_ARROW = 225, 
   WINGDING_DOWN_ARROW = 226
};

/* +--------------------------------------------------------------------------+
/* Stores general context about a chart, agnostic of any trade signals
 *  This context includes info such as : 
 *     - In which direction is the chart trending? eg TREND_DIRECTION_BEARISH
 *     - What are the SR levels at different time frames? 
 */
 
class CChartContext {
   private: 
   
      int TrendThreshold;
      bool UseTrendToggle;
      CChartObjectButton m_button;
      
      CChartObjectLabel* TrendLabel;   
      CChartObjectLabel* DeltaLabel; 
      CChartObjectLabel* FlowLabel;
      
      SRLevels* DailySRLevels;
      SRLevels* WeeklySRLevels;
      CiMA* LongerTermMA;
      CiMA* ShorterTermMA; 
      
      void refreshTrendLabel(void);
      void refreshTrendDirection(void);
   public: 
      
      ~CChartContext() {
         delete TrendLabel;
         delete DeltaLabel;
         delete FlowLabel;
         delete LongerTermMA;
         delete ShorterTermMA;
      }
      
      bool shouldTradeLowerTimeframe(ENUM_TIMEFRAMES period);
      
      ENUM_WINGDING getTrendWingding();
      ENUM_TREND_DIRECTION getTrendDirection();
      void refresh();
            
      SRLevels* getWeeklySRLevel() {return WeeklySRLevels;}
      SRLevels* getDailySRLevel() {return DailySRLevels;}
      
      CChartContext* setDailySRLevels(SRLevels& sr_levels) {
         DailySRLevels = &sr_levels;
         return (&this);
      }
      
      CChartContext* setWeeklySRLevels(SRLevels& sr_levels) {
         WeeklySRLevels = &sr_levels;
         return (&this);
      }
      
      CChartContext* setShorterTermMA(CiMA* ma) {
         delete ShorterTermMA;
         ShorterTermMA = ma;
         return (&this);
      }
      
      CChartContext* setLongerTermMA(CiMA* ma) {
         delete LongerTermMA;
         LongerTermMA = ma;
         return (&this);
      }
      
      CChartContext * setTrendThreshold(int trend_threshold) {
         TrendThreshold = trend_threshold;
         return (&this);
      }
      
      CChartContext* useTrendToggle() {
         UseTrendToggle = true;         
         return (&this);
      }
      
      CChartContext* load() {
         TrendLabel = new CChartObjectLabel();
         DeltaLabel = new CChartObjectLabel();
         FlowLabel = new CChartObjectLabel();
         refresh();
         return (&this);
      }
      bool OnEvent(const int id,const long &lparam,const double &dparam,const string &sparam) {
         if(sparam == TREND_BUTTON_NAME) {
            refreshTrendDirection();
            return true;
         }
         return false;
      }
      
      void OnTick() {
         refreshTrendDirection();
      }
      
      void updateTrendLabel(string text, const int clr) {
         TrendLabel.Description(text);
         TrendLabel.Color(clr);
      }
};

bool CChartContext::shouldTradeLowerTimeframe(ENUM_TIMEFRAMES period) {

  double ma21 =  iMA(Symbol(), period, 21, 0, MODE_SMA, PRICE_CLOSE, 0);
  double ma14 = iMA(Symbol(), period, 14, 0, MODE_SMA, PRICE_CLOSE, 0);
  bool properBuy = (getTrendDirection() == TREND_DIRECTION_BULLISH) && Ask > ma14 && ma14 > ma21;
  bool properSell = getTrendDirection() == TREND_DIRECTION_BEARISH && Bid < ma14 && ma14 < ma21;
  return properBuy || properSell; 
  
}

ENUM_WINGDING CChartContext::getTrendWingding() {
   ENUM_TREND_DIRECTION direction = getTrendDirection();
   switch(direction) {
      case TREND_DIRECTION_SIDEWAYS: 
         return WINGDING_STRAIGHT_FACE; 
      case TREND_DIRECTION_BEARISH: 
         return WINGDING_DOWN_ARROW; 
      case TREND_DIRECTION_BULLISH: 
         return WINGDING_UP_ARROW;
   }
   return (ENUM_WINGDING) Utils::exception(
      StringFormat(
         "Unexpected value %d recieved for ENUM_TREND_DIRECTION", direction
      ),
      __FILE__,
      __LINE__
   );
}

void CChartContext::refresh() {
   DailySRLevels.refresh();
   WeeklySRLevels.refresh();
   refreshTrendDirection();
   // Drawing labels crashes the optimizer. Plus we don't need them for optimization anyways 
   // if (IsTesting()) return;
   
   // When we change chart templates, this erases all chart objects. So we have to 
   // poll for its existence on every refresh and re-draw if necessary
   if (ObjectFind(ChartID(), TREND_OBJECT_NAME) < 0) {
      bool result = TrendLabel.Create(ChartID(), TREND_OBJECT_NAME, 0, 75, 25);
      if (result == False) {
         Utils::exception("Could not create label. Naming conflict?", __FILE__, __LINE__);
      } 
      TrendLabel.Corner(CORNER_RIGHT_UPPER); 
      TrendLabel.Anchor(ANCHOR_RIGHT);
      TrendLabel.FontSize(22); 
      TrendLabel.Hidden(False);
   }
   
   
   if (ObjectFind(ChartID(), TREND_BUTTON_NAME) < 0) {
      m_button.Create(ChartID(), TREND_BUTTON_NAME, 0, 60, 15, 45, 25);
      m_button.Description("Toggle");
      ObjectSetInteger(ChartID(), TREND_BUTTON_NAME, OBJPROP_CORNER, CORNER_RIGHT_UPPER);
   }
   
   if(ObjectFind(ChartID(), FLOW_OBJECT_NAME) < 0) {
      bool result = FlowLabel.Create(ChartID(), FLOW_OBJECT_NAME, 0, 75, 75);
      FlowLabel.Corner(CORNER_RIGHT_UPPER);
      FlowLabel.Anchor(ANCHOR_RIGHT);
      FlowLabel.FontSize(22);
      FlowLabel.Hidden(False);
   }
   if(shouldTradeLowerTimeframe(PERIOD_D1)) {
      FlowLabel.Description("FLOW");
   } else {
      FlowLabel.Description("NO FLOW");
   }
   
}

void CChartContext::refreshTrendDirection(void) {
   ENUM_TREND_DIRECTION direction = getTrendDirection();
   switch (direction) {
      case(TREND_DIRECTION_SIDEWAYS):
         updateTrendLabel("SIDEWAYS", clrGray);
         break;
      case(TREND_DIRECTION_BULLISH): 
         updateTrendLabel("BUY", clrGreen);
         break;
      case(TREND_DIRECTION_BEARISH): 
         updateTrendLabel("SELL", clrRed);
         break;
      default: 
         Utils::exception(
            StringFormat(
               "Unexpected value %d recieved for ENUM_TREND_DIRECTION", direction
            ),
            __FILE__,
            __LINE__
         );
   }
 }
 
ENUM_TREND_DIRECTION CChartContext::getTrendDirection() {
   
   if(UseTrendToggle) {
      if(m_button.State()) {
         return TREND_DIRECTION_BEARISH;
      } else {
         return TREND_DIRECTION_BULLISH;
      }
   }
   
   double delta = LongerTermMA.GetData(0) - ShorterTermMA.GetData(0);
   
   // if they're too close together, the market is in sideways movement. 
   // don't trade any exponentials 
   double pip_delta = (int)(delta / Point);
   
   if (ObjectFind(ChartID(), TREND_DELTA_NAME) < 0) {
      bool result = DeltaLabel.Create(ChartID(), TREND_DELTA_NAME, 0, 50, 80);
      if (result == False && !IsTesting()) {
         Utils::exception("Could not create label. Naming conflict?", __FILE__, __LINE__);
      }
      DeltaLabel.Corner(CORNER_RIGHT_UPPER); 
      DeltaLabel.Anchor(ANCHOR_RIGHT);
      DeltaLabel.FontSize(22); 
      DeltaLabel.Hidden(False);
   }
   DeltaLabel.Description((string) pip_delta);
   
   if (MathAbs(pip_delta) < TrendThreshold) {
      return TREND_DIRECTION_SIDEWAYS;
   // if the longer term ma has a higher value, the market has been moving 
   // downward, so it's bearish. 
   } else if (delta > 0) {
      return TREND_DIRECTION_BEARISH;
   // otherwise the shorter term ma has a higher value, so the market has
   // been moving upward. It's bullish. 
   } else {
      return TREND_DIRECTION_BULLISH; 
   }
}
      