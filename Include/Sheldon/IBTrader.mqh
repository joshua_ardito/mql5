//+------------------------------------------------------------------+
//|                                                    IB trader.mq4 |
//|                                    Copyright 2017, Joshua Ardito |
//|                                                                  |
//+------------------------------------------------------------------+

#include <Indicators/Trend.mqh>
#include <Trader.mqh>
#include <MTMore/Utils.mqh>

class IBTrader: public CMockableTrader {
   
   private:
      CiMA *MA1;
      CiMA *MA2;
      bool TradeBothSides;
      int TrendThreshold;
      
     double getDelta() {
         return (open(SlowdownLookback +1) - close (2)) / point();
      }
     
   public:
      
      // inherited from CTrader
      virtual bool isExponential();
      virtual bool isValidSignal();
      virtual void getSignalInfo(
         s_signal_info& signal_info
      );
      
      virtual string getSignalStringName() {
         return "IB";
      }
      
      virtual ENUM_SIGNAL_TYPE getSignalType() {
         return SIGNAL_IB;
      }
      
      virtual void setSpecificExampleParameters() {
         TradeBothSides = False;
         TrendThreshold = 400;
      }
      
      // Setters
      void setTradeBothSides(bool trade_both_sides) {
         TradeBothSides = trade_both_sides;
      }
      
      void setTrendThreshold(int trend_threshold) {
        TrendThreshold = trend_threshold;
      }
      
      void setMAs(CiMA* ma1, CiMA* ma2) {
         MA1 = ma1;
         MA2 = ma2;
      }
};

bool IBTrader::isValidSignal() {
   bool res = (high(1) < high(2) && low(1) > low(2));
   return res;
} 

// Defined in CTrader
void IBTrader::getSignalInfo(
   s_signal_info& signal_info
) {
   bool do_buy = false, do_sell = false;
   
   ENUM_RISK_TYPE risk = RISK_STABLE;
   if (TradeBothSides) {
      signal_info.display_message = "Trading both sides";
      do_buy = true;   
      do_sell = true;
   }
   
   // Trade using the moving average
   if (MA1 != NULL && MA2 != NULL) {
      if (MA1.GetData(0) > MA2.GetData(0)) {
         signal_info.display_message = "MA1 > MA2";
         do_buy = true;
      } else {
         signal_info.display_message = "MA1 <= MA2";
         do_sell = true;
      }
    // Trade using the trend threshold
   } else {
      if (getDelta() > 0) {
         do_sell = true;
      } else {
         do_buy = true;
      }
   }
   
   if (do_buy && !do_sell) {
      signal_info.direction = DIRECTION_BUY;
   } else if (do_sell && !do_buy) {
      signal_info.direction = DIRECTION_SELL;
   } else if (do_sell && do_buy) {
      signal_info.direction = DIRECTION_ANY;
   } else {
      Utils::exception("Expected to do either buy or sell", __FILE__, __LINE__);
   }
}



bool IBTrader::isExponential() {
   // if market movement is strong, riskit for the biskit
   return (MathAbs(getDelta()) > TrendThreshold);
}
//+------------------------------------------------------------------+