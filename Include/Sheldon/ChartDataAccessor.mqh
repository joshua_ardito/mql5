//+------------------------------------------------------------------+
//|                                                  CBaseTrader.mqh |
//|                        Copyright 2018, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#include<MTMore/Utils.mqh> 
#include<MTMore/Order.mqh> 

//+-------------------------------------------------------------------+
//| This should serve as a base class for any class that accesses data| 
//| from a chart. By wrapping functions that traditionally access     |
//| chart data, we can override them in mocks and pass test data to   | 
//| them instead.                                                     |
//+-------------------------------------------------------------------+

class CChartDataAccessor {

      protected: 
         bool Buying;
         ENUM_TIMEFRAMES m_period;
         void CChartDataAccessor::OrientAroundOrder(COrder* order);
         
      // basics
      virtual double high(int i=1) {
         return iHigh(Symbol(), m_period, i);
      }
      
      virtual double low(int i=1) {
         return iLow(Symbol(), m_period, i);
      }
      
      virtual double open(int i=1) {
         return iOpen(Symbol(), m_period, i);
      }
      
      virtual double close(int i=1) {
         return iClose(Symbol(), m_period, i);
      }
      
      virtual double size(int i=1) {
         return high() - low();
      }
      
      virtual datetime time(int i=1) {
         return iTime(Symbol(), m_period, i);
      }
      
      virtual double point() {
         return Point;
      }
      
      virtual double digits() {
         return Digits; 
      }
        
      virtual datetime timeCurrent() {
         return TimeCurrent();
      }
      // These are helper functions for simplifying logic around whether 
      // a trade is a buy or a sell. Instead, we can refer to the
      //  profitDirection agnostic of it's actual numeric direction. 
      // the profitBoundary agnostic of its absolute boundary (high or low) 
      // and vice versa. 
      double profitBoundary() {
         if (Buying) {
            return high();
         } else {
            return low();
         }
      }
      
      double lossBoundary() {
         if (Buying) {
            return low();
         } else {
            return high();
         }
      }
      
      int profitDirection() {
         if (Buying) {
            return 1;
         } else {
            return -1;
         }
      }
      
      int lossDirection() {
         if (Buying) {
            return -1;
         } else {
            return 1;
         }
      }
      
      // a > b for buy, a < b for sell 
      bool inProfitDirection(double a, double b) {
         if (Buying) {
            return a > b;
         } else {
            return a < b;
         }
      }
      
      bool inLossDirection(double a, double b) {
         if (Buying) {
            return a < b;
         } else {
            return a > b;
         }
      }
    public: 
      void setBuying(bool buying) {
         Buying = buying;
      }
      
      void setDirection(ENUM_ORDER_TYPE order_type) {
         // flip the context so we can think of everything in terms of
         // profit and loss directions (instead of bigger numbers or lower numbers
         if (order_type == OP_BUY) {
            Buying = True;
         } else if (order_type == OP_SELL) {
            Buying = False; 
         } else {
            Utils::exception(
               "Expected either buy or sell when creating and sending orders",
               __FILE__, 
               __LINE__
            ); 
         }
      }
};

void CChartDataAccessor::OrientAroundOrder(COrder* order) {
   if (order.OrderType_() == OP_BUY) {
      Buying = True;
   } else if(order.OrderType_() == OP_SELL) {
      Buying = False;
   } else {
      Alert("Got unexpected value for order type");
      ExpertRemove();
   }
}