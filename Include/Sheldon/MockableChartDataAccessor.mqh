//+------------------------------------------------------------------+
//|                                   CMockableChartDataAccessor.mqh |
//|                        Copyright 2018, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict

#include<MTMore/TestLib.mqh> 
#include<ChartDataAccessor.mqh> 


/* Ideally, we would mock any class that accesses chart data by overriding the data access 
 * methods in a mock class. However, we generally want to mock different classes in the same
 * exact way, and since MQL4 does not allow multiple inheritance, this would mean a lot of 
 * copy-pasting of code. Hence, this clowny code that instead cases on whether or not we are
 * mocking this instance, and mimicks the mock functionality instead of returning real data.
 */ 
 
class CMockableChartDataAccessor: public CChartDataAccessor {
   protected: 
      s_mock_bar MockedBars[];  
      bool Mocking;
      
   public: 
       
      CMockableChartDataAccessor() {
         Mocking = False;
      }
      
      void setMockedBar(int i, s_mock_bar& mocked_bar) {
         if(ArraySize(MockedBars) <= i) {
            ArrayResize(MockedBars, i);
         }
         
         MockedBars[i] = mocked_bar;
      }
      
      void setMockedBars(s_mock_bar& mocked_bars[]) {
         ArrayResize(MockedBars, ArraySize(mocked_bars));
         ArrayCopy(MockedBars, mocked_bars);
      }
      
      virtual double high(int i=1) {
         if (Mocking) {
            return MockedBars[i].high;
         } else {
            return CChartDataAccessor::high(i);
         };
      }
      
      virtual double low(int i=1) {
         if (Mocking) {
            return MockedBars[i].low;
         } else {
            return CChartDataAccessor::low(i);
         }
      }
      
      virtual double open(int i=1) {
         if (Mocking) {
            return MockedBars[i].open;
         } else {
            return CChartDataAccessor::open(i);
         }
      }
      
      virtual double close(int i=0) {
         if (Mocking) {
            return MockedBars[i].close;
         } else {
            return CChartDataAccessor::close(i);
         }
      }
      
      virtual double point() {
         return 0.00001;
      }
      
      virtual double digits() {
         if (Mocking) {
            return 5;
         } else {
            return CChartDataAccessor::digits();
         }
      }
      
      void useMock() {
         Mocking = True;
      }
};