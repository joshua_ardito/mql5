//+------------------------------------------------------------------+
//|                                                  PinBarTrader.mq4|
//|                                    Copyright 2017, Joshua Ardito |
//|                                                                  |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2017, Joshua Ardito"
#property link      ""
#property version   "1.00"
#property strict

#include<Indicators/Trend.mqh>
#include<Trader.mqh>

#define SHAPE_SHIFT 300

enum ENUM_PIN_BAR {
   PIN_UP,
   PIN_DOWN,
   PIN_NONE
};

class PinBarTrader final: public CMockableTrader {

private:

   double PinBarPercent;
   int SlowdownThreshold;
   int RetracementLowerBound;
   int RetracementUpperBound;
   int RetracementLookback;
   
   bool isRetracement();
public:
   // simple functions
   virtual bool isExponential();
   virtual bool isValidSignal();
   virtual void getSignalInfo(
      s_signal_info& signal_info
   );       
   
   virtual string getSignalStringName() {
      return "PIN BAR";
   }
   
   virtual ENUM_SIGNAL_TYPE getSignalType() {
     return SIGNAL_PIN_BAR;
   }
   
   virtual void setSpecificExampleParameters() {
      PinBarPercent = 65;
      SlowdownThreshold = 400;
   }
 
   void setPinBarPercent(double pin_bar_percent) {
      PinBarPercent = pin_bar_percent;
   }
   
   void setSLBufferPercent(double sl_buffer_percent) {
      SLBufferPercent = sl_buffer_percent;
   }
   
   void setEntryPercent(double entry_percent) {
      EntryPercent = entry_percent;
   }
   
   void setSlowdownThreshold(int slowdown_threshold) {
      SlowdownThreshold = slowdown_threshold;
   }
   
   void setRetracementLowerBound(int retracement_lower_bound) {
      RetracementLowerBound = retracement_lower_bound;
   }
   
   void setRetracementUpperBound(int retracement_upper_bound) {
      RetracementUpperBound = retracement_upper_bound;
   }
   
   void setRetracementLookback(int retracement_lookback) {
      RetracementLookback = retracement_lookback;
   }
   
   void setPinEntryPercent(double entry_percent) {
      EntryPercent = entry_percent;
   }
   
   ENUM_PIN_BAR getPinBarType ();   
   
   bool isOnSr(double& sr_list[]);
   ENUM_DIRECTION setOrderIfPinBar(double& daily_sr[], double& weekly_sr[], ENUM_DIRECTION direction);
      
      
};

bool PinBarTrader::isValidSignal() {
   return getPinBarType() != PIN_NONE;
}

ENUM_PIN_BAR PinBarTrader::getPinBarType () {

   int i = 1;
   double top, bottom;
   double size = high(i) - low(i);
   double mid = (high(i) + low(i)) / 2;
   
   if (open(i) > close(i)) {
      top = open(i);
      bottom = close(i);
   } else {
      top = close(i);
      bottom = open(i);
   }
   
   if ((high(i) - top) / size  > PinBarPercent && top < mid) {
      return PIN_DOWN;
   } else if ((bottom - low(i)) / size > PinBarPercent && bottom > mid) {
      return PIN_UP;
   } else {
      return PIN_NONE;
   }
}

bool PinBarTrader::isOnSr(double& sr_list[]) {

   double low = iLow(Symbol(), m_period, 1);
   double high = iHigh(Symbol(), m_period, 1);
   
   for (int  i = 0; i < ArraySize(sr_list); i++) {
      /* pin bar falls on an SR level */
      if (low < sr_list[i] && high > sr_list[i]) {
         return true;
      }
   }   
   return false;
}

void PinBarTrader::getSignalInfo(
   s_signal_info& signal_info
) {
   
   // sanity check: if most recent bar is not pin bar, don't trade
   ENUM_PIN_BAR pin_bar = getPinBarType();
   
   int i = 1;
   double top, bottom;
   double size = high(i) - low(i);
   double mid = (high(i) + low(i)) / 2;
   
   if (open(i) > close(i)) {
      top = open(i);
      bottom = close(i);
   } else {
      top = close(i);
      bottom = open(i);
   }
   
   if ((high(i) - top) / size  > PinBarPercent && top < mid) {
      signal_info.display_message = StringFormat("bottom: %.6f mid: %.6f top: %.6f", bottom, mid, top);
   } else if ((bottom - low(i)) / size > PinBarPercent && bottom > mid) {
      signal_info.display_message = StringFormat("bottom: %.6f mid: %.6f top: %.6f", bottom, mid, top);
   } 
   
   
   ENUM_DIRECTION direction = DIRECTION_NONE;
   double delta;
   datetime time = iTime(Symbol(), m_period, 1);
   string message = "";
   double tps[];
   
   delta = MathAbs(open( SlowdownLookback + 1) - close(2)) / point();
   
   // if pin is not on slowdown, don't trade
   if(delta > SlowdownThreshold) {
      signal_info.display_message = "No slowdown";
      signal_info.direction = DIRECTION_NONE;
   } else if (pin_bar == PIN_UP) {
      signal_info.direction = DIRECTION_BUY;
   } else if (pin_bar == PIN_DOWN) {
      signal_info.direction = DIRECTION_SELL;
   } else {
      Utils::exception("Expected either pin up or pin down", __FILE__, __LINE__);
   }
}

bool PinBarTrader::isRetracement() {
   ENUM_PIN_BAR type = getPinBarType();
   
   switch (type) {
      case PIN_NONE: 
         return (bool) Utils::exception("Got no signal for pin, expected signal.", __FILE__, __LINE__);
      case PIN_UP:
         Buying = True;
         break; 
      case PIN_DOWN:
         Buying = False;
         break;
      default: 
         return (bool) Utils::exception("Got unexpected value for pin bar type.", __FILE__, __LINE__);
   }
   
   double range_open = open(RetracementLookback + 1); 
   double range_close = close(2);
   double delta = MathAbs(range_close - range_open) / point();
 
   if (inLossDirection(range_close, range_open) && delta > RetracementLowerBound && delta < RetracementUpperBound) {
      return true;
   } else {
      return false;
   }
   
}

bool PinBarTrader::isExponential() {
   return true;
}