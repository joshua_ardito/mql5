//+------------------------------------------------------------------+
//|                                         CSheldonOrderManager.mqh |
//|                                    Copyright 2018, Joshua Ardito |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, Joshua Ardito"
#property strict

#include <MTMore/OrderManager.mqh> 
#include <MTMore/Utils.mqh> 

#define POST_URI "https://maker.ifttt.com/trigger/sheldon_notif/with/key/cU1eJuq7DJSDqjkVs9rxxh"

struct s_sr_target {
   int offset; 
   double trail_percent; 
};

enum ENUM_RISK_TYPE {
   RISK_EXPONENTIAL = 1,
   RISK_STABLE = 2
};

enum ENUM_SIGNAL_TYPE {
   SIGNAL_PIN_BAR = 1,
   SIGNAL_IB = 2
};


class CSheldonOrder : public COrder 
   {
private: 
   ENUM_RISK_TYPE m_risk_type; 
   ENUM_TIMEFRAMES m_period; 
   ENUM_SIGNAL_TYPE m_signal; 
   
public: 
   ENUM_RISK_TYPE RiskType(void) {return(m_risk_type); }
   void RiskType(ENUM_RISK_TYPE risk_type) {m_risk_type = risk_type; }
   
   ENUM_TIMEFRAMES Period(void) {return (m_period); }
   void Period(ENUM_TIMEFRAMES period) {m_period = period; }
   
   ENUM_SIGNAL_TYPE Signal(void) {return (m_signal); }
   void Signal(ENUM_SIGNAL_TYPE signal) {m_signal = signal; }
};
   
class CSheldonOrderManager : public COrderManager {

   private:
      bool TradeStable;
      double StableRisk;
      double ExponentialRisk;
      s_sr_target SRTargets[];
      int GetBaseLevel(COrder* order, double& sr_levels[]);
      
      void flattenSRs(double& daily_sr[], double& weekly_sr[], double& flattened_sr[]);
      double CSheldonOrderManager::getTP(
         CSheldonOrder* order,
         int base_level,
         s_sr_target& target,
         double& sr_levels[]
      );
      
   public:
      void writeOrders();

      void sendOrders(
         CSheldonOrder* order,
         double& daily_sr[],
         double& weekly_sr[],
         double fib_tp,
         int expiration_hrs
      );

      void setTradeStable (bool trade_stable) {TradeStable = trade_stable;}
      void setStableRisk (double stable_risk) {StableRisk = stable_risk;}
      void setExponentialRisk (double exponential_risk) {ExponentialRisk = exponential_risk;}
      
      void addSRTarget(s_sr_target& sr_target) { 
         int old_size = ArraySize(SRTargets);
         bool res = ArrayResize(SRTargets, old_size + 1);
         SRTargets[old_size] = sr_target;
      }
      
      void setSRTargets(int& sr_targets[]) {
         ArrayResize(SRTargets, ArraySize(sr_targets));
         ArrayCopy(SRTargets, sr_targets);
      }
      
      void setExampleParameters() {
         m_acceptable_ratio = 1; 
         StableRisk = 5;
         ExponentialRisk = 15;
         m_slippage = 3;
         ArrayResize(SRTargets, 2);
         s_sr_target t0 = {1, 0};
         s_sr_target t1 = {2, 0};
         SRTargets[0] = t0;
         SRTargets[1] = t1;
         TradeStable = true;
      }
      
      static CSheldonOrder* MakeCopies(CSheldonOrder* order);
      
};

int CSheldonOrderManager::GetBaseLevel(COrder* order, double& sr_levels[]) {
   OrientAroundOrder(order);

   // find first TP from entry
   int base_level = ArrayBsearch(sr_levels, order.OpenPrice());
   if (inLossDirection(sr_levels[base_level], order.OpenPrice())) {
      base_level += 1 * profitDirection();
   }
   return base_level;
}

/* Given a sorted array of daily and weekly sr levels, flattens the sr levels 
 * such that the new flattened array contains all daily sr levels and all weekly 
 * sr levels that are lower than the lowest daily and higher than the highest daily
 */
void CSheldonOrderManager::flattenSRs(
   double& daily_sr[], 
   double& weekly_sr[],
   double& flattened_sr[]
) {
   int i = 0;
   while(i < ArraySize(weekly_sr) && weekly_sr[i] < daily_sr[0]) {
      Utils::ArrayAppend(flattened_sr, weekly_sr[i]);
      i++;
   }
   
   ArrayCopy(flattened_sr, daily_sr, ArraySize(flattened_sr), 0);
   
   
   while(i < ArraySize(weekly_sr) && weekly_sr[i] > daily_sr[ArraySize(daily_sr)-1]) {
      Utils::ArrayAppend(flattened_sr, weekly_sr[i]);
      i++;
   }
   
}

void CSheldonOrderManager::sendOrders(
   CSheldonOrder* base_order,
   double& daily_sr[],
   double& weekly_sr[],
   double fib_tp,
   int expiration_hrs
) {

   double total_risk = StableRisk;
   
   double tps[];
   double flattened_sr[];
   
   flattenSRs(daily_sr, weekly_sr, flattened_sr);
   
   Utils::ArrayAppend(tps, fib_tp);
   // if we are given no tps we can't send any orders so
   // just return
   if (ArraySize(tps) == 0) {
      Print ("Not sending orders due to no tps found");
   };
   
   if (base_order.RiskType() == RISK_EXPONENTIAL) {
      total_risk = ExponentialRisk;
   } else if (TradeStable == false) {
      Print("Not trading stable risk signal");
      return;
   }
   
   // populate base order 
   base_order.Slippage(m_slippage); 
   if(expiration_hrs != 0) {
      RelativeTime* time = new RelativeTime();
      time.Hours(expiration_hrs);
      base_order.SetExpirationRelativeToOpen(time);
      delete time;
   }
   int base_level = GetBaseLevel(base_order, flattened_sr);
   
   CSheldonOrder* orders[]; 
   
   // create orders for all SR Targets with valid TP levels
   for(int i=0; i < ArraySize(SRTargets); i++) { 
      PrintFormat("%d", SRTargets[i].offset);
      double tp = getTP(base_order, base_level, SRTargets[i], flattened_sr);
      Print("Take Profit: ", tp);
      if (tp == -1) {
         continue;
      }
      CSheldonOrder* order = new CSheldonOrder(); 
      SheldonOrder::CopyFrom(base_order, order);
      order.TakeProfit(tp);
      order.TrailPips((int)((size() * (SRTargets[i].trail_percent / 100)) / point()));

      bool res = order.DetermineOrderType();
      if (res == False) {
         Utils::exception("Could not determine order type:", __FILE__, __LINE__);
      }
      Utils::ArrayAppend(orders, order);
   }
   
   // add fib order
   CSheldonOrder* fib_order = new CSheldonOrder(); 
   SheldonOrder::CopyFrom(base_order, fib_order); 
   fib_order.TakeProfit(fib_tp);
   bool res = fib_order.DetermineOrderType();
   if (res == False) {
      Utils::exception("Could not determine order type:", __FILE__, __LINE__);
   }
      
   Utils::ArrayAppend(orders, fib_order);
   
   double risk_percent  = total_risk / ArraySize(orders);   
   
   for(int i = 0; i < ArraySize(orders); i++) {
      // distribute risk evenly across all targets
      orders[i].SetPercentageRisk(risk_percent);
      recordAndSendOrder(orders[i]);
   }
     const char data[] = {};
     char result[];
     string result_headers;
     int timeout = 0;
     // send phone call when trade is sent
     int response = WebRequest("POST", POST_URI, "", timeout, data, result, result_headers);

}

double CSheldonOrderManager::getTP(
   CSheldonOrder* order,
   int base_level,
   s_sr_target& target,
   double& sr_levels[]
) {
   
      // SR Target of 0 is equivalent to no SR target at all
   if (target.offset <= 0 ) {
      return -1;
   }         
   
   int index = base_level + ((target.offset - 1) * profitDirection());
   
   // normalize index to last SR level
   if(index < 0) {
      index = 0;
   } else if (index >= ArraySize(sr_levels)) {
      index = ArraySize(sr_levels) - 1;
   }

   // validate tp
   double pip_risk = MathAbs(order.OpenPrice() - order.StopLoss());
    if (pip_risk <= (MIN_PIP_RISK * point())) {
      return -1;
    }
    
   double tp = sr_levels[index];
   double pip_profit = MathAbs(order.OpenPrice() - tp);
   if ((pip_profit / pip_risk) < m_acceptable_ratio) {
      return -1;
   }
   
   return tp;
}

void SafePrint(int num, string numDescriptor, int denom, string denomDescriptor) {
   if (denom == 0) {
      Print ("No ",denomDescriptor);
   } else {
      Print ("% of ", numDescriptor,": ", float(num) / denom * 100);
   }
}

void CSheldonOrderManager::writeOrders(void) {

   int exponential_total = 0; 
   int stable_total = 0; 
   int exponential_win = 0; 
   int stable_win = 0; 
   
   int pin_bar_total = 0;
   int pin_bar_win = 0; 
   
   int ib_total = 0; 
   int ib_win = 0;
   
   int exponential_ib_win = 0;
   int exponential_ib_total = 0; 
   
   int exponential_pin_win =0; 
   int exponential_pin_total = 0;
   
   int trade_total = 0;
   
   // Open file for writing 
   int file_handle = FileOpen("orders.csv", FILE_WRITE | FILE_CSV, ',');
   if (file_handle == INVALID_HANDLE) {
      Alert("Failed opening file 'orders.csv' for writing"); 
      return;
   }
   
   for(CSheldonOrder* o = AllOrders.GetFirstNode(); o != NULL; o = AllOrders.GetNextNode()) {
   
      int res = OrderSelect(o.Ticket(), SELECT_BY_TICKET, MODE_HISTORY);
      ENUM_HISTORY_TYPE order_history_type = OrderHistoryType();
      
      // Write order data to file, we can do more complex data analysis with this. 
      uint openRes = FileWrite(
         file_handle, 
         o.Ticket(), 
         EnumToString(o.RiskType()), 
         EnumToString(o.Signal()), 
         EnumToString(order_history_type),
         EnumToString((ENUM_ORDER_TYPE)OrderType()),
         EnumToString(o.Period())
      );
      
      if (openRes == 0) {
         Alert("Error writing order with ticket number ", o.Ticket(), "to file.");
      }
      
      if (o.Ticket() == -1) {
         continue;
      }
      
      trade_total +=1;
      // sanity checks
      if (o.TakeProfit() != OrderTakeProfit()) {
         Print("Found inconsistent order tp. Expected ", o.TakeProfit(), ". Actual: ", OrderTakeProfit());
      }
      
      if (order_history_type == HISTORY_TP) {
         if (o.Signal() == SIGNAL_PIN_BAR) {
            pin_bar_win +=1;
            if (o.RiskType() == RISK_EXPONENTIAL) {
               exponential_pin_win +=1;
            }
         } else if (o.Signal() == SIGNAL_IB){
            ib_win +=1;
            if (o.RiskType() == RISK_EXPONENTIAL) {
               exponential_ib_win +=1;
            }
         }
         
         if (o.RiskType() == RISK_EXPONENTIAL) {
            exponential_win +=1;
         } else if (o.RiskType() == RISK_STABLE){
            stable_win +=1 ;
         }
      }
      
      if (o.Signal() == SIGNAL_PIN_BAR) {
         pin_bar_total += 1;
      } else if (o.Signal() == SIGNAL_IB) {
         ib_total +=1;
      }
      
      if (o.RiskType() == RISK_EXPONENTIAL) {
         exponential_total +=1;
         if (o.Signal() == SIGNAL_IB) {
            exponential_ib_total +=1;
         } else if(o.Signal() == SIGNAL_PIN_BAR) {
            exponential_pin_total +=1;
         }
      } else if (o.RiskType() == RISK_STABLE) {
         stable_total +=1;
      }
      
   }
   FileClose(file_handle);
   SafePrint(exponential_total, "exponential trades set", (exponential_total + stable_total), "total trades set");
   SafePrint(exponential_win, "exponential trades won", exponential_total, "exponential trades");
   SafePrint(stable_win, "stable trades won", stable_total, "stable trades");
   SafePrint(pin_bar_total, "pin bar trades set", trade_total, "trades set");
   SafePrint(pin_bar_win, "pin bar trades won", pin_bar_total, "pin bar trades"); 
   SafePrint(ib_win, "ib trades won", ib_total, "ib trades");
   SafePrint(exponential_ib_win, "ib exponential won", exponential_ib_total, "ib exponential trades"); 
   SafePrint(exponential_pin_win, "pin exponential won", exponential_pin_total, "pin exponential trades");
}

class SheldonOrder 
   {
public:
   static void CopyFrom(CSheldonOrder* src_order, CSheldonOrder* dst_order);
};


void SheldonOrder::CopyFrom(CSheldonOrder* src_order, CSheldonOrder* dst_order) {
   src_order.CopyInto(dst_order);
   dst_order.RiskType(src_order.RiskType());
   dst_order.Period(src_order.Period());
   dst_order.Signal(src_order.Signal());
}