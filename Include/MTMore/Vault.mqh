//+------------------------------------------------------------------+
//|                                                        Vault.mqh |
//|                                    Copyright 2018, Joshua Ardito |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, Joshua Ardito"
#property strict

#include <Files/FileBin.mqh>
#include <MTMore/Order.mqh> 

#define MS_TO_SLEEP 500
#define MAX_RETRIES 5

class CVault
   {
protected: 
   string m_filename;
   CFileBin* m_file;
   int OpenWithRetries();
   int Modify(double profit_or_loss);
   int SelectOrder(COrder* order); 
   
public: 
   CVault(string filename = "exposed_balance.dat") {
      m_filename = filename;
      m_file = new CFileBin();
   }
   
   int Add(double profit);
   int AddOrderProfits(COrder* order, double percentage);
   
   int Subtract(double loss);   
};

int CVault::OpenWithRetries() {
   int retries = 0;
   int handle = INVALID_HANDLE;
   while (retries < MAX_RETRIES) {
      handle = m_file.Open(m_filename, FILE_READ | FILE_WRITE);
      if (handle == INVALID_HANDLE && GetLastError() == ERR_FILE_CANNOT_OPEN) {
         Sleep(MS_TO_SLEEP); 
      } else {
         break;
      }
   }
   return handle;
}

int CVault::Add(double profit) {
   return Modify(profit);
}

int CVault::Subtract(double loss){
   return Modify(loss * -1);
}
int CVault::Modify(double profit_or_loss) {
   int handle = OpenWithRetries(); 
   if (handle == INVALID_HANDLE) {
      return -1;
   }
   double current_exposed; 
   m_file.ReadDouble(current_exposed); 
   double proposed_exposed = current_exposed + profit_or_loss;
   
   if (proposed_exposed > AccountBalance()) {
      return exception("Exposed balance cannot be greater than actual account balance"); 
   }
   m_file.WriteDouble(proposed_exposed);
   return 1;
}

int CVault::SelectOrder(COrder* order) {
   if (order.Ticket() == -1) {
      return exception("Could not find order ticket. Order either failed or was never sent", __FILE__, __LINE__); 
   }
   if(OrderSelect(order.Ticket(), SELECT_BY_TICKET, MODE_HISTORY) == false) {
      return exception("Error trying to find the order in history. Perhaps it hasn't yet closed? ", __FILE__, __LINE__);
   }
   return 0;
}

int CVault::AddOrderProfits(COrder* order, double percentage = 100) {
   SelectOrder(order);
   if (OrderProfit() <= 0) {
      exception(StringFormat("Expected order to result in a profit, got: ", OrderProfit()), __FILE__, __LINE__);
   }
   Add(OrderProfit());
   return 0;
}     