//+------------------------------------------------------------------+
//|                                                       Order.mqh |
//|                                    Copyright 2018, Joshua Ardito |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, Joshua Ardito"
#property strict

#include<Object.mqh>
#include<MTMore/RelativeTime.mqh> 
#include<MTmore/Utils.mqh>

#define DEFAULT_TRAIL_THRESHOLD 20

/* The possible directions that a single order can be in. 
 * It's either a sell, (tp below entry), or a buy (tp above entry). 
 */
enum ENUM_ORDER_DIRECTION {
   ORDER_DIRECTION_BUY,
   ORDER_DIRECTION_SELL
};


class COrder: public CObject 
   {
protected: 

   //-- variables crucial to the OrderSend function. 
   //   These must all be set in order to successfully send the order
   
   string m_symbol;
   
   ENUM_ORDER_TYPE m_order_type;
   
   double m_open_price;
   double m_stop_loss;
   double m_take_profit;
   double m_lots;
   
   int m_magic;
   int m_slippage;
   int m_ticket;
   
   string m_comment;
   
   datetime m_expiration;
   int m_exp_sec_relative_to_open; 
   
   // -- other variables 
   int m_trail_pips;
   int m_trail_threshold; 
   bool m_sent;
   
   string GetValidationErrorMessage();
public: 
   //-- constructor 
    COrder() {
      m_symbol= Symbol();
      
      m_open_price = -1; 
      m_stop_loss = -1; 
      m_take_profit = -1; 
      m_lots = -1; 
      
      m_magic = -1; 
      m_slippage = -1; 
      m_ticket = -1; 
      
      m_comment = "";
      m_expiration = -1; 
      m_exp_sec_relative_to_open = -1;
      
      m_trail_threshold = DEFAULT_TRAIL_THRESHOLD;
      m_sent = false;
   }
          
   static COrder* GetFromSelectedOrder();          
   //--- methods to access protected data
   //                  ~CObject(void)                                       {                 }
   int             Ticket(void) const { return(m_ticket);}
   
   string Symbol(void) {return m_symbol; }
   void   Symbol(string symbol) {m_symbol = symbol; }
   
   template<typename T> 
   void foo(T bar) {
      Print("test");
   }
   ENUM_ORDER_TYPE OrderType_() {return(m_order_type); }
   void            OrderType_(ENUM_ORDER_TYPE order_type)    { m_order_type=order_type;    }
   bool            HasOrderType(void) {return m_order_type != -1; }
   double          OpenPrice(void) const { return(m_open_price);}
   void            OpenPrice(double open_price) {m_open_price = open_price;}
   bool            HasOpenPrice(void) const {return m_open_price != -1; };
   
   double          StopLoss(void) const { return(m_stop_loss);}
   void            StopLoss(double stop_loss) {m_stop_loss = stop_loss;}
   bool            HasStopLoss(void) const {return m_stop_loss != -1; }
   
   double          TakeProfit(void) const { return(m_take_profit);}
   void            TakeProfit(double take_profit) {m_take_profit = take_profit;}
   bool            HasTakeProfit(void) const {return m_take_profit != -1; }
   
   double          Lots(void) const { return(m_lots);}
   void            Lots(double lots) {m_lots = lots;}
   bool            HasLots(void) {return m_lots != -1; }
   
   int             Magic(void) const { return(m_magic);}
   void            Magic(int magic) {m_magic = magic;}
   
   int             Slippage(void) const { return(m_slippage);}
   void            Slippage(int slippage) {m_slippage = slippage;}
   bool            HasSlippage(void) {return m_slippage != -1;}
   
   
   string          Comment(void) const { return(m_comment);}
   void            Comment(string comment) {m_comment = comment;}
   
   datetime        Expiration(void) const  { return(m_expiration);}
   void            Expiration(datetime expiration) {m_expiration = expiration;}
   
   void            TrailPips(int trail_pips) {m_trail_pips = trail_pips; }
   int             TrailPips(void) {return m_trail_pips; }
   
   void            TrailThreshold(int trail_threshold) {m_trail_threshold = trail_threshold;}
   int             TrailThreshold(void) {return m_trail_threshold; }
   
   bool            Sent(void) {return m_sent;}
   //-- methods to calculate data primitives intelligently
   void           SetPercentageRisk(double percentage);
   void           SetExpirationRelativeToOpen(RelativeTime* time); 
   bool           DetermineOrderType(); 
   
   //--- methods to dynamically calculate data derived from primitives
   double GetRiskAmount(); 
   double GetRewardAmount();
   double COrder::GetRiskRewardRatio();
   
   double        GetMarketPrice();
   //-- method of sending an order 
   int           Send(void);
   //--- method of comparing objects
   virtual int       Compare(const CObject *node,const int mode=0) const; 
   //-- method of validating that an order's parameters are complete enough to be sent to the OrderSend function 
   bool          IsValid(void);
   void          AssertValid(void);
   //---methods of dealing with trailing stops 
   bool          UpdateTrail();
   //--method of evaluating the state of an order 
   bool          IsInProfit(double new_sl);
   
   //--method of copying data
   void COrder::CopyInto(COrder* dst_order);
      
};

COrder* COrder::GetFromSelectedOrder(void) {
   COrder* order = new COrder();
   order.TakeProfit(OrderTakeProfit());
   order.StopLoss(OrderStopLoss());
   order.OpenPrice(OrderOpenPrice());
   order.Lots(OrderLots());
   order.OrderType_((ENUM_ORDER_TYPE) OrderType());
   order.Symbol(OrderSymbol());
   return order;
}

string COrder::GetValidationErrorMessage(void) {
   string message = ""; 
   if (!HasStopLoss()) message += "No stop loss provided.";
   if (!HasOpenPrice()) message += "No open price provided.";
   if (!HasTakeProfit()) message += "No take profit provided."; 
   if (!HasLots()) message += "No lots provided."; 
   if (!HasOrderType()) message += "No order type provided";
   if (!HasSlippage()) message += "No slippage provided";
   
   if (m_expiration != -1 && m_exp_sec_relative_to_open != -1) {
      message += "Specified both a relative and absolute expiration. Only specify either one or the other";
   }
   
   return message;
};

bool COrder::IsValid(void) {
   string message = GetValidationErrorMessage();
   return message == "";
}

void COrder::AssertValid(void) {
   string message = GetValidationErrorMessage(); 
   if (message != "") {
      Utils::exception("Encountered errors in validating order: " + message, __FILE__, __LINE__);
   }
}

double COrder::GetMarketPrice(void) {
   return Order::GetMarketPriceFromDirection(Order::GetDirectionFromType(m_order_type));
}

void COrder::SetPercentageRisk(double risk_percentage) {
   if (m_open_price == -1) {
      Utils::exception("Cannot set volume as percentage risk if open price not set");
   }
   
   if (m_stop_loss == -1) {
      Utils::exception("Cannot set volume as percentae risk if stop loss not set");
   }
   
   if (m_stop_loss == 0) {
      Utils::exception("Cannot set volume as percentage risk with stop loss of 0", __FILE__, __LINE__);
   }
   
   if (risk_percentage <= 0) {
      Utils::exception(StringFormat("Cannot set volume as percentage risk of: %s", risk_percentage), __FILE__, __LINE__);
   }

   if (m_open_price == m_stop_loss) {
      return;
   }
   double risk_pips = MathAbs(m_open_price - m_stop_loss) / MarketInfo(m_symbol, MODE_POINT);
   double balance_risk = (risk_percentage / 100) * AccountBalance();
   double lots = balance_risk / risk_pips;
   m_lots = Order::NormalizeLots(lots);
}

void COrder::SetExpirationRelativeToOpen(RelativeTime* time) {
   m_exp_sec_relative_to_open = time.ToSeconds();
}

bool COrder::DetermineOrderType(void) {

   PrintFormat("Take Profit: %.6f, Open Price: %.6f, Stop Loss: %.6f", m_take_profit, m_open_price, m_stop_loss);
   if (!HasOpenPrice() || !HasTakeProfit() || !HasStopLoss()) {
      return false;
   }

   double stoplevel = MarketInfo(Symbol(), MODE_STOPLEVEL) * Point;
   
   // this is a buy order
   if (m_stop_loss < m_open_price && m_open_price < m_take_profit) {
      if (Ask - stoplevel > m_open_price) {
         m_order_type = OP_BUYLIMIT;
      } else if (Ask + stoplevel < m_open_price) {
         m_order_type = OP_BUYSTOP;
      } else {
         m_order_type = OP_BUY;
         m_open_price = Ask;
      }
      return true;
   } else if (m_stop_loss > m_open_price && m_open_price > m_take_profit) {
      if (Bid + stoplevel < m_open_price) {
         m_order_type = OP_SELLLIMIT;
      } else if (Bid - stoplevel > m_open_price ) {
         m_order_type = OP_SELLSTOP;
      } else {
         m_order_type = OP_SELL;
      }
      return true;
   }
   // order type could not be determined because sl and tp 
   // are not on opposite sides of the open price
   return false;
}

int COrder::Send() {
   datetime expiration_date;
   
   Print ("Sending order");
   Print(m_expiration);
   if (!IsValid()) {
      return Utils::exception("Order is not valid", __FILE__, __LINE__);
   }
   // don't try to resend an order that is already sent
   if (m_sent == true) {
      return Utils::exception("Order already sent", __FILE__, __LINE__);
   }
   
   if (m_expiration != -1) {
      expiration_date = m_expiration;
   } else if (m_exp_sec_relative_to_open != -1){
      expiration_date = TimeCurrent() + m_exp_sec_relative_to_open;
   } else {
      expiration_date = 0;
   }
   
   m_ticket = OrderSend(
      m_symbol, 
      m_order_type,
      m_lots, 
      m_open_price, 
      m_slippage, 
      m_stop_loss, 
      m_take_profit, 
      m_comment, 
      m_magic, 
      expiration_date
   );
   
   if (m_ticket == -1) {
      double market_price = Order::GetMarketPriceFromDirection(
         Order::GetDirectionFromType(m_order_type)
      );
      Alert(StringFormat(
         "Failed to send order. Lots: %.2f, Open Price: %.6f SL: %.6f, TP: %.6f, Market Price: %.6f, Order Type: %s, Expiration: %d", 
         m_lots, 
         m_open_price, 
         m_stop_loss, 
         m_take_profit,
         market_price, 
         EnumToString(m_order_type),
         expiration_date
      ));
   }
   m_sent = true;
   return m_ticket;
}

bool COrder::UpdateTrail() {
   /* check the return values of these things? */
   if(OrderSelect(m_ticket, SELECT_BY_TICKET) == false) {
      return false;
   }   
   
   double dist, new_sl;
   bool res;
   double trail = m_trail_pips * Point;
   
   /* can't trail by 0 or negative pips */
   if (m_trail_pips <= 0) {
      return false;
   }
   
   if (OrderType() == OP_BUY) {
      dist =  Ask - OrderStopLoss();
      new_sl = Ask - trail;
   } else if (OrderType() == OP_SELL) {
      dist = OrderStopLoss() - Bid;
      new_sl = Bid + trail;
   } else {
      /* can only activate trail on buy and sell orders */
      return (bool) Utils::exception("NO!", __FILE__, __LINE__);;
   }
  
   if (dist - trail > m_trail_threshold * Point && IsInProfit(new_sl)) {
      /* we don't check res, the callsite of this function should immediately check error codes */
      res = OrderModify(OrderTicket(), OrderOpenPrice(), new_sl, OrderTakeProfit(), 0);
   }
   return true;
}

bool COrder::IsInProfit(double new_sl) {
   if(OrderType() == OP_BUY && OrderOpenPrice() <= new_sl) return true;
   else if (OrderType() == OP_SELL && OrderOpenPrice() >= new_sl) return true; 
   return false;
}

int COrder::Compare(const CObject *node,const int mode=0) const {
   const COrder* order = node; 
   return order.Ticket() - m_ticket;
}

void COrder::CopyInto(COrder* dst_order) {

   dst_order.Symbol(m_symbol);
   
   dst_order.OpenPrice(m_open_price); 
   dst_order.StopLoss(m_stop_loss); 
   dst_order.TakeProfit(m_take_profit); 
   dst_order.Lots(m_lots); 
   
   dst_order.Slippage(m_slippage); 
   
   dst_order.Comment(m_comment);
   dst_order.Expiration(m_expiration);
   dst_order.TrailPips(m_trail_pips);
   dst_order.TrailThreshold(m_trail_threshold);
   if(m_expiration != -1) {
      dst_order.Expiration(m_expiration);
   } else if (m_exp_sec_relative_to_open != -1) {
      RelativeTime* time = new RelativeTime(); 
      time.Seconds(m_exp_sec_relative_to_open);
      dst_order.SetExpirationRelativeToOpen(time);
      delete time;
   }
}

double COrder::GetRewardAmount(void) {
   if(m_take_profit == 0) {
      return -1;
   }
   return NormalizeDouble(m_lots * Utils::GetPipDifference(m_take_profit, m_open_price, m_symbol), 2);
}

double COrder::GetRiskAmount(void) {
   if(m_stop_loss == 0) {
      return -1;
   }
   return NormalizeDouble(m_lots * Utils::GetPipDifference(m_stop_loss, m_open_price, m_symbol), 2);
}

double COrder::GetRiskRewardRatio(void) {
   double reward_amount = GetRewardAmount();
   double risk_amount = GetRiskAmount();
   if(reward_amount == -1 || risk_amount == -1 || risk_amount == 0) {
      return -1;
   } 
   return NormalizeDouble(reward_amount/risk_amount, 2);
}

class CSearchOrder : public COrder 
   {
   public: 
      void Ticket(const int ticket) {m_ticket = ticket; }
};

class Order final {
   public:
      static double               GetMarketPriceFromDirection(ENUM_ORDER_DIRECTION direction);
      static ENUM_ORDER_DIRECTION GetDirectionFromType(ENUM_ORDER_TYPE order_type);
      static double               NormalizeLots(double lots);
};

static double Order::GetMarketPriceFromDirection(ENUM_ORDER_DIRECTION direction) {
   switch(direction) {
      case (ORDER_DIRECTION_BUY): 
         return Bid;
      case (ORDER_DIRECTION_SELL): 
         return Ask;
      default: 
         return (ENUM_ORDER_DIRECTION) Utils::exception("got unexpected value for order direction", __FILE__, __LINE__);
   }
}

static double Order::NormalizeLots(double lots) {

   // Most brokers only allow lots with up to 2 decimal points
   lots = NormalizeDouble(lots, 2);
   double brokerMaxLot = MarketInfo(Symbol(), MODE_MAXLOT);
   double maxLotBasedOnMargin= AccountFreeMargin() / MarketInfo(Symbol(),MODE_MARGINREQUIRED);
   double maxlot = NormalizeDouble(MathMin(brokerMaxLot, maxLotBasedOnMargin), 2);
   double minlot = MarketInfo(Symbol(), MODE_MINLOT);
   
   if (lots > maxlot) {
      Alert("Lots too big");
      return maxlot;
   } else if (lots < minlot) {
      Alert("Lots too small");
      return minlot;
   } else {
      return lots;
   }
}

static ENUM_ORDER_DIRECTION Order::GetDirectionFromType(ENUM_ORDER_TYPE order_type) {
   switch (order_type) {
      case(OP_BUY): 
      case(OP_BUYLIMIT): 
      case(OP_BUYSTOP): 
         return ORDER_DIRECTION_BUY;
      case(OP_SELL): 
      case(OP_SELLLIMIT): 
      case(OP_SELLSTOP): 
         return ORDER_DIRECTION_SELL;
      default: 
         return (ENUM_ORDER_DIRECTION) Utils::exception("got unexpected value for order type", __FILE__, __LINE__);
   }
}  