//+------------------------------------------------------------------+
//|                                                       Assert.mqh |
//|                        Copyright 2018, Joshua Ardito             |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018,Joshua Ardito"
#property strict

struct s_mock_bar {
   double high;
   double low;
   double open;
   double close;
};

interface ITest {
   void run();
};

string DoubleArrayToString(double& array[]) {
   string str = "";
   for(int i = 0; i < ArraySize(array); i++) {
      str += DoubleToStr(array[i]);
   }
   return str;
}

void verifyBar(s_mock_bar& bar) {
   if (
      bar.low > bar.open || 
      bar.low > bar.close || 
      bar.low > bar.high) {
      Alert("Invalid bar. Something higher than the low");   
   }
   
   if (
      bar.high < bar.open || 
      bar.high < bar.close || 
      bar.high < bar.low
   ) {
      Alert("Invalid bar. Something is lower than the low");
   }
}
      
class Expecter {
   protected: 
      string Filename;
      int LineNumber;
      void getError(string actual, string expected) {
         Alert(
            "Failure in file ", Filename, " on line ", LineNumber, 
            ": Expected ", expected, " got ", actual
         );
      }
};

class BoolExpecter : public Expecter {
   private:
      bool Statement;
      
   public: 
      BoolExpecter(bool statement, int line_number, string filename) {
         Filename = filename;
         LineNumber = line_number;
         Statement = statement;
      };
      
      void toBeTrue() {
       if (Statement != True) {
         getError("False", "True");
       }
       delete (&this);  
      };
      
      void toBeFalse() {
         if (Statement != False) {
            getError("True", "False");
         }
         delete (&this);
      };
};

class IntExpecter: public Expecter {

   private: 
      int ActualInt;
      
   public: 
      IntExpecter(int actual_int, int line_number, string filename) {
         Filename = filename;
         LineNumber = line_number;
         ActualInt = actual_int;
      };
      void toEqual(int expected) {
         if (ActualInt != expected) getError((string) ActualInt, (string) expected);
         delete (&this);
      }
};

class DoubleExpecter: public Expecter {

   private: 
      double ActualDouble;
      
   public: 
      DoubleExpecter(double actual_double, int line_number, string filename) {
         Filename = filename;
         LineNumber = line_number;
         ActualDouble = actual_double;
      };
      
      void toEqual(double expected) {
         if (expected != ActualDouble) getError((string) ActualDouble, (string) expected);
         delete (&this);
      }
};

class DoubleArrayExpecter: public Expecter {
   private: 
      double ActualDoubleArray[];
   public:
      DoubleArrayExpecter(double& actual[], int line_number, string filename){
         Filename = filename;
         LineNumber = line_number; 
         ArrayResize(ActualDoubleArray, ArraySize(actual));
         ArrayCopy(ActualDoubleArray, actual);
      }; 
      
      void toEqual(double& expected[]) {
         if (!isEqual(ActualDoubleArray, expected)) {
            getError(
               DoubleArrayToString(ActualDoubleArray), 
               DoubleArrayToString(expected)
            );
         }
         delete (&this);
      };
      
};
 
bool isEqual(double& actual[], double& expected[]) {
   
   if (ArraySize(actual) != ArraySize(expected)) {
     return false;
   }
   
   if(ArraySize(actual) == 0) {
      return true;
   }
   
   ArraySort(actual);
   ArraySort(expected);
   for (int i = 0; i < ArraySize(actual); i++) {
      if (actual[i] != expected[i]) {
        return false;
      }
   }
   return true;
}

class Asserter {

   protected:
      string Filename;
      
   public: 
      // Initialize the tester with the name of the 
      // file
      Asserter(string filename) {
         Filename = filename;
      }
      
      DoubleArrayExpecter* expect(double& actual[], int line_number) {
         return (new DoubleArrayExpecter(actual, line_number, Filename));
      }
      
      DoubleExpecter* expect(double actual, int line_number) {
         return (new DoubleExpecter(actual, line_number, Filename));
      }
      
      BoolExpecter* expect(bool statement, int line_number) {
         return new BoolExpecter(statement, line_number, Filename);
      }
      
      IntExpecter* expect(int actual, int line_number) {
         return new IntExpecter(actual, line_number, Filename);
      }
};