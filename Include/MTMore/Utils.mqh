//+------------------------------------------------------------------+
//|                                                        Utils.mqh |
//|                                     Copyright 2018, Joshua Ardito|
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, Joshua Ardito"
#property link      "https://www.mql5.com"
#property strict

enum ENUM_DIRECTION {
   DIRECTION_NONE,
   DIRECTION_BUY,
   DIRECTION_SELL,
   DIRECTION_ANY
};

enum ENUM_ERRTYPE{   
   ERRTYPE_FATAL,
   ERRTYPE_RETRY,
   ERRTYPE_WAIT_BEFORE_RETRY,
   ERRTYPE_GENERAL
};

enum ENUM_MSG{
   MSG_CONTEXT_BUSY,
   MSG_EXPERT_TERMINATED,
   MSG_CONTEXT_FREE,
   MSG_TRADE_NOT_ALLOWED,
   MSG_NOT_CONNECTED,
   MSG_EXPERT_NOT_ENABLED
};


enum ENUM_HISTORY_TYPE {
  HISTORY_FAILED,
  HISTORY_EXPIRED,
  HISTORY_SL,
  HISTORY_TP,
  HISTORY_UNKNOWN
};


class Utils 
   {
public:   
   template<typename T>
   static bool ArrayAppend(T &array[], T item);
   static int exception(string message, string filename="", int line_number=0);
   static string Utils::TradeTypeText(int t);
   static int GetPipDifference(double l1, double l2, string symbol_);
};

template<typename T>
static bool Utils::ArrayAppend(T &array[], T item) {
   int old_size = ArraySize(array);
   bool res = ArrayResize(array, old_size + 1);
   array[old_size] = item;
   return res;
}

string GetMsg(ENUM_MSG msg){
   switch(msg) {
      case MSG_CONTEXT_BUSY:
         return (
            "Broker trade contextt is busy!" +
            "EA will wait until it is free ..."
         );
      case MSG_EXPERT_TERMINATED:
         return "The expert was terminated by the user!";
      case MSG_CONTEXT_FREE:
         return ( 
            "Broker trade context has become free!" + 
            "EA will continue to perform trading actions"
         );
      case MSG_TRADE_NOT_ALLOWED:
         return (
            ": Trade is not allowed. EA cannot run. Please check \"Allow live trading\"" +
            "in the \"Common\" tab of the EA properties window"
         );
      case MSG_NOT_CONNECTED:
         return (
            "No connection with broker server! " +
            "EA will start running once connection established"
         );
      case MSG_EXPERT_NOT_ENABLED:
         return (
            "Please enable \"Expert Advisors\" in the top toolbar of Metatrader to run this EA"
         );
        
      default:
         return "Msg not found";
   }
}

// It doesn't seem like you can throw exceptions in MQL4 so this is 
// my hacky remedy
static int Utils::exception(string message, string filename="", int line_number = 0) {

   string full_message = "";
   if (filename != "") {
      full_message += StringFormat("Exception in file %s", filename);
   }
   
   if(line_number != 0) {
      full_message += StringFormat(" on line %d", line_number);
   }
   
   full_message += StringFormat(": %s", message);
   Alert(full_message);
   
   // if we threw an exception from the context of an EA, this will 
   // make sure the expert is removed from the chart. 
   ExpertRemove();
   int i = 0;
   
   // this will force a div by zero error to stop the backtester, 
   // since it will ignore the ExpertRemove function. 
   double result = 1 / i;
   return -1;
}


static int Utils::GetPipDifference(double l1, double l2, string symbol_) {
   return(int) (MathAbs(l1 - l2) / MarketInfo(symbol_, MODE_POINT));
}
   
ENUM_HISTORY_TYPE OrderHistoryType() {
   if (StringFind(OrderComment(), "tp") != -1) {
      return HISTORY_TP;
   } else if (StringFind(OrderComment(), "sl") != -1) {
      return HISTORY_SL;
   } else if (StringFind(OrderComment(), "expiration") != -1) {
      return HISTORY_EXPIRED;
   } else if (OrderTicket() == -1) {
      return HISTORY_FAILED;
   }
   
   Print("Could not categorize order with comment of ", OrderComment(), "with expiration", OrderExpiration(), "and ticket", OrderTicket());
   return HISTORY_UNKNOWN;
}

ENUM_ERRTYPE GetErrorType(int err) {
   switch(err) {
      case ERR_TRADE_TIMEOUT: 
      case ERR_INVALID_PRICE: 
      case ERR_INVALID_STOPS:
      case ERR_OFF_QUOTES: 
      case ERR_TRADE_MODIFY_DENIED: 
      case ERR_TRADE_CONTEXT_BUSY:
         return ERRTYPE_WAIT_BEFORE_RETRY;
         
      case ERR_OLD_VERSION:
      case ERR_NOT_ENOUGH_RIGHTS:
      case ERR_ACCOUNT_DISABLED:
      case ERR_INVALID_ACCOUNT:
         return ERRTYPE_FATAL;
         
      case ERR_NO_ERROR:
      case ERR_SERVER_BUSY:
      case ERR_NO_CONNECTION:
      case ERR_PRICE_CHANGED:
      case ERR_BROKER_BUSY:
      case ERR_REQUOTE:
         return ERRTYPE_RETRY;
        
      default:
         return ERRTYPE_GENERAL;
   }
}

/* This function has to be used very frequently to truncate calculated values to price with the correct
 * amount of decimal places, this is a shortened version to be used for convenience */
double nd(double value) {
   return NormalizeDouble(value,Digits);
}

string Utils::TradeTypeText(int t) { 
   switch(t) { 
      case OP_BUY: return("BUY"); 
      case OP_SELL: return("SELL"); 
      case OP_BUYLIMIT: return("BUY LIMIT"); 
      case OP_SELLLIMIT: return("SELL LIMIT"); 
      case OP_BUYSTOP: return("BUY STOP"); 
      case OP_SELLSTOP: return("SELL STOP"); 
   } 
   return("N/A"); 
}

/* Warning, this function uses a static variable but takes a period parameter.
 * use it only for 1 given period within a single program for desired effect */
bool NewBar(ENUM_TIMEFRAMES period){
   static datetime lastbar;
   datetime currbar = iTime(Symbol(), period, 0);
   if(lastbar != currbar){
      lastbar = currbar;
      return(true);
   }
   else {
      return false;
   }
}  

double getBarSize(int i) {
   return (High[i] - Low[i]) / Point;
}


string TimeframeToString(ENUM_TIMEFRAMES timeframe) {
   switch(timeframe) {
      case(PERIOD_H1): 
         return "H1";
      case(PERIOD_H4):
         return "H4"; 
      case(PERIOD_D1): 
         return "Daily";
      case(PERIOD_W1): 
         return "Weekly";
      default: 
         return EnumToString(timeframe);
   }
}