//+------------------------------------------------------------------+
//|                                                      SRUtils.mq4 |
//|                                    Copyright 2017, Joshua Ardito |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property library
#property copyright "Copyright 2017, Joshua Ardito"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include <ChartObjects\ChartObjectsLines.mqh>
#include <MTMore/Utils.mqh> 

class SRLevels {

protected: 
   CChartObjectHLine* SRLevelLines[];
   double SRLevelValues[];
   
private:
   ENUM_TIMEFRAMES m_period;
   int Colour;
   int PipTolerance;
   int Lookback;
   int HitThresh;
   string SRName;
   
   void deleteLines();
public:
   
   ~SRLevels() {
      deleteLines();
   }
   
   SRLevels* setPeriod(ENUM_TIMEFRAMES period) {
      m_period = period; 
      return (&this);
   }
   
   SRLevels* setColour(int colour) {
      Colour = colour; 
      return (&this);
   }
   
   SRLevels* setTolerance(int tolerance) {
      PipTolerance = tolerance; 
      return (&this);
   }
   
   SRLevels* setLookback(int lookback) {
      Lookback = lookback; 
      return (&this);
   }
   
   
   SRLevels* setHitThresh(int hit_thresh) {
      HitThresh = hit_thresh; 
      return (&this);
   }
   
   SRLevels* setSRName(string sr_name) {
      SRName = sr_name;
      return (&this);
   }
   
   void getSRLevelValues(double& dst_array[]) {
      ArrayResize(dst_array, ArraySize(SRLevelValues));
      ArrayCopy(dst_array, SRLevelValues);
   }
   
   double getLevelAt(int i) {
      return SRLevelValues[i];
   }
   
   void draw();
   void calculate();
   virtual void refresh();
};
    
void SRLevels::draw() {
   ArrayResize(SRLevelLines, ArraySize(SRLevelValues));
   for(int i = 0; i < ArraySize(SRLevelValues); i++) {
      string obj_name = SRName + IntegerToString(i);
      CChartObjectHLine* sr_level = new CChartObjectHLine();
      sr_level.Create(ChartID(), obj_name, 0, SRLevelValues[i]);
      sr_level.Color(Colour); 
      sr_level.Selectable(false);
      SRLevelLines[i] = sr_level;
   }
}

void SRLevels::deleteLines() {
   for (int i = 0; i < ArraySize(SRLevelLines); i++) {
      delete SRLevelLines[i];
   }
}
void SRLevels::refresh() {
   /* delete old SR lines and generate new ones based on params */
   deleteLines();
   calculate();
   draw();
}

void SRLevels::calculate(){
   
   double turns[];
   double test_level, test_level_sum = 0;
   int level_hits = 1;
   double tolerance = PipTolerance * Point();
   ArrayResize(turns, Lookback * 2);
   /* stash local maxima and minima */
   
   for(int i=0; i < Lookback; i++) {
      turns[2*i] = iHigh(Symbol(), m_period, i+1);
      turns[(2*i)+1] = iLow(Symbol(), m_period, i+1);
   }
   ArraySort(turns);
   ArrayResize(SRLevelValues, 0);
   for(int i=0; i < ArraySize(turns); i ++) {
   
      /* We are starting a new test level */
      if(test_level_sum == 0) {
         test_level_sum = turns[i];
         level_hits = 1;
         continue;
      }
      
      test_level = test_level_sum / level_hits;
      
      
      /* if we found a turning point close to our test_level */
      if(MathAbs(turns[i] -  test_level) < tolerance){
         level_hits += 1;
         test_level_sum += turns[i];
         
      } 
      /* otherwise we start a new test_level */
      else { 
         if(level_hits > HitThresh) {
            Utils::ArrayAppend(SRLevelValues, test_level);
         }
         test_level_sum = 0;
      }
   }
   
   // normalize levels
   for (int i = 0; i < ArraySize(SRLevelValues); i++) {
      SRLevelValues[i] = nd(SRLevelValues[i]);
   }
};

class SRLevelsMock: public SRLevels {

public:
   // Dont do anything on refresh. 
   virtual void refresh() {
      return;
   }
   
   SRLevelsMock* setSRLevelValues(double& src_array[]) {
      ArrayResize(SRLevelValues, ArraySize(src_array));
      ArrayCopy(SRLevelValues, src_array);
      return (&this);
   }
};