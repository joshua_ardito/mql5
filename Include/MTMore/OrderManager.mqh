//+------------------------------------------------------------------+
//|                                                  COrderManager.mqh |
//|                                    Copyright 2017, Joshua Ardito |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Joshua Ardito"
#property strict

#include <ChartDataAccessor.mqh> 
#include <Object.mqh> 
#include <Arrays/List.mqh> 
#include <MTMore/Utils.mqh> 

#include<MTMore/Order.mqh>

// don't set any trades under 20 pips
#define MIN_PIP_RISK 20

// trail every 25 pips
#define TRAIL_THRESH 25

class COrderManager : public CChartDataAccessor {
   protected: 
   
      double m_acceptable_ratio;
      int m_slippage;
      bool IsAboveEntry(double new_sl);
      CList* AllOrders;
      CList* LastOpenOrders; 
      
      void recordAndSendOrder(COrder* order);
      
   public:
      COrderManager(void) {
         AllOrders = new CList();
      }
      
      ~COrderManager(void) {
         COrder* o = AllOrders.GetFirstNode();
         while(o != NULL) {
            o = AllOrders.GetNextNode();
         }
         delete AllOrders;
      }
      
      // Update the trailing stop of all active trades, if necessary
      void UpdateActiveTrails();
      
      // If provided, the OrderManager will not set any trades below this risk/reward ratio. 
      // TODO: Actually implement this 
      void AcceptableRatio(double acceptable_ratio) {m_acceptable_ratio = acceptable_ratio;}
      
      // If provided, this slippage will be used as a default slippage across all orders 
      // sent my this OrderManager
      void Slippage (int slippage) {m_slippage = slippage;}
     
};

void COrderManager::UpdateActiveTrails () {
   bool res;
   for (int i=0; i < OrdersTotal(); i++) {
      // Create dummy order with the order's magic number set so we can find the
      // actual order data with that ticket #
      res = OrderSelect(i,SELECT_BY_POS);
      CSearchOrder* dummy_order = new CSearchOrder();
      dummy_order.Ticket(OrderTicket());
      if (OrderType() == OP_BUY || OrderType() == OP_SELL) {
         COrder* actualOrder = AllOrders.Search(dummy_order);
         if (actualOrder == NULL) {
            Utils::exception(StringFormat("Couldn't find order with ticket #%d", OrderTicket()), __FILE__, __LINE__);
         }
         actualOrder.UpdateTrail();
      }
      delete dummy_order;
   }
}

void COrderManager::recordAndSendOrder(COrder* order) {
   Print("Sending Order");
   if(order.Send() == -1) {
      Alert("Error sending order");
   };
   AllOrders.Add(order);
   AllOrders.Sort(0);
}