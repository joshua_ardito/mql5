//+------------------------------------------------------------------+
//|                                                 AbsoluteTime.mqh |
//|                                    Copyright 2018, Joshua Ardito |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2018, Joshua Ardito"
#property link      "https://www.mql5.com"
#property strict

#define SEC_IN_MIN 60
#define MIN_IN_HOUR 60
#define HOUR_IN_DAY 24

class RelativeTime 
   {
   protected: 
      int m_seconds; 
      int m_minutes; 
      int m_hours; 
      int m_days;
      
   public: 
      int Seconds() {return m_seconds; }
      void Seconds(int seconds) {m_seconds = seconds;}
      int Minutes() {return m_minutes; }
      void Minutes(int minutes) {m_minutes = minutes; }
      int Hours() {return m_hours; }
      void Hours(int hours) {m_hours = hours;}
      int Days() {return m_days; }
      void Days(int days) {m_days = days;};
       
      int ToSeconds();
};

int RelativeTime::ToSeconds() {
   int day_seconds = m_days * HOUR_IN_DAY * MIN_IN_HOUR * SEC_IN_MIN; 
   int hour_seconds = m_hours * MIN_IN_HOUR * SEC_IN_MIN; 
   int minute_seconds = m_minutes * SEC_IN_MIN; 
   
   return day_seconds + hour_seconds + minute_seconds + m_seconds;
}