//+------------------------------------------------------------------+
//|                                                    IBExpert.mq4  |
//|                                    Copyright 2017, Joshua Ardito |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Joshua Ardito"
#property link      ""
#property version   "1.00"
#property strict

#define APP_NAME     "IB Trader"
#define APP_VERSION  "1.0"
#define SECONDS_IN_DAY 86400

/* Amount in seconds that the OnTimer() event will take to fire */
#define TIMER_DELAY 600
#define DAILY_SR_NAME "DAILY"

#include <stdlib.mqh>
#include <arditolib.mqh>
#include<Indicators/Trend.mqh>
#include<SRLevels.mqh>
#include<IBTrader.mqh>

/* ------------------------- External Settings Start -------------------------*/

/*General Settings */
extern const int Slippage = 3;
extern const double AcceptableRatio = 1.5;
extern const bool SetOnBothSides = false;
extern const double Pin_Bar_Percent = 0.6;

/*Daily Settings */
extern const double EntryBufferPercent = 15;
extern const double SLBufferPercent = 15;
extern const double ExponentialRisk = 20;
extern const double StableRisk = 20;
extern const double TPPercent = 261.8;
extern const double TrailPercent = 120;
extern const int ExpirationHrs = 24;
extern const int SRTarget = 2;

/* SR detection settings */
extern const int Tolerance = 20;
extern const int HitThreshold = 4;
extern const int Lookback = 200;

/* MA1 Settings */
extern const ENUM_APPLIED_PRICE MA1AppliedPrice = 0;
extern const ENUM_MA_METHOD MA1Method = 0;
extern const int MA1Period = 5;
extern const int MA1Shift = 0;

/* MA2 Settings */
extern const ENUM_APPLIED_PRICE MA2AppliedPrice = 0;
extern const ENUM_MA_METHOD MA2Method = 0;
extern const int MA2Period = 14;
extern const int MA2Shift = 0;

/* Fib settings */

/* Used to simulate OnTimer firing in testing */
datetime lastOnTimerExecution;

datetime lastDailyBar, lastH4Bar;
bool isDailyIB = False;
CiMA *MA1 = new CiMA();
CiMA *MA2 = new CiMA();
IBTrader *ibTrader;

SRLevels *dailySR;
/* ------------------------- External Settings End ---------------------------*/

void EAAlert(string s) {
   Alert(APP_NAME, " v",APP_VERSION,": ",s);
}

int InitFail(string err_msg) {
   EAAlert(err_msg);
   return(INIT_FAILED);
}

int OnInit()
  {
   if (!IsConnected()) return InitFail(GetMsg(MSG_NOT_CONNECTED));
   if (!IsExpertEnabled()) return InitFail(GetMsg(MSG_EXPERT_NOT_ENABLED));
   if (!IsTradeAllowed()) return InitFail(GetMsg(MSG_TRADE_NOT_ALLOWED));
   bool res = EventSetTimer(TIMER_DELAY);
   if (IsTesting()) lastOnTimerExecution = TimeCurrent();
   Alert("Timer initialized!");
   
   MA1.Create(Symbol(), PERIOD_D1, MA1Period, MA1Shift, MA1Method, MA1AppliedPrice);
   MA1.Create(Symbol(), PERIOD_D1, MA2Period, MA2Shift, MA2Method, MA2AppliedPrice);
   ibTrader = new IBTrader(
      StableRisk,
      ExponentialRisk,
      SLBufferPercent,
      TPPercent,
      EntryBufferPercent,
      TrailPercent,
      SRTarget,
      AcceptableRatio,
      Slippage,
      ExpirationHrs,
      SetOnBothSides
   );
   
   dailySR = new SRLevels(PERIOD_D1, clrRed, Tolerance, Lookback, HitThreshold, DAILY_SR_NAME);
   
   return(INIT_SUCCEEDED);
  }
  
void OnDeinit(const int reason)
  {
   if (UninitializeReason() == REASON_CHARTCHANGE) return;
   Comment("");
   delete dailySR;
   delete ibTrader;
   
  }

void OnTimer() {
   CheckTrail();
}

void OnTick()
{
   
   /* OnTimer event does not fire in the backtester so we have to test it in this hacky way */
   if (IsTesting() &&  TimeCurrent() > lastOnTimerExecution + TIMER_DELAY){
      OnTimer();
      lastOnTimerExecution = TimeCurrent();
   }
   
   if(NewBar(PERIOD_H4)) {
      double sr_levels[];
      dailySR.calculateSR(sr_levels);
      /* if the new bar opens on a new day, we have a new daily bar as well*/
      if(TimeHour(iTime(Symbol(), PERIOD_H4, 0)) == 0) {
         isDailyIB = ibTrader.setOrderIfIB(PERIOD_D1, sr_levels, MA1, MA2);
      }
      
      ibTrader.setOrderIfIB(PERIOD_H4, sr_levels, MA1, MA2);
   }
}
   
//+------------------------------------------------------------------+
