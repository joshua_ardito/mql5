//+------------------------------------------------------------------+
//|                                               Pin Bar Trader.mq4 |
//|                                    Copyright 2017, Joshua Ardito |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Joshua Ardito"
#property link      ""
#property version   "1.00"
#property strict

#define APP_NAME     "Pin Bar Trader"
#define APP_VERSION  "1.0"
#define SECONDS_IN_DAY 86400
#define TRAIL_THRESH 20

#define DAILY_SR_NAME "DAILY"
#define WEEKLY_SR_NAME "WEEKLY"
#define PINNAME "pinbar"

/* Amount in seconds that the OnTimer() event will take to fire */
#define TIMER_DELAY 600
#define SHAPE_SHIFT 300

#include <stdlib.mqh>
#include <arditolib.mqh>
#include "IBTrader.mq4"

/* Used to simulate OnTimer firing in testing */
datetime lastOnTimerExecution;

/* Slowdown Settings */
extern const double Delta = 2100;
extern const int SlowdownLookback = 2;

extern const double PinBarPercent = 0.65;
extern const double TrailPercent = 110;

/* Money Management settings */
extern const double ExponentialRisk = 30;
extern const double StableRisk = 5;
extern const double AcceptableRatio = 1;

/* Order Settings */
extern const double TakeProfitPercent = 261;
extern const int SR_Target = 2;
extern const int Slippage = 3;
extern const double Entry_Percent = 70;
extern const double SL_Buffer_Percent = 15;
extern const int expiration_hrs = 24;

/* SR Detection Settings */
extern const int Weekly_Tolerance = 1500;
extern const int Daily_Tolerance = 600;
extern const int Lookback = 500;
extern const int HitThresh = 15;



void EAAlert(string s) {
   Alert(APP_NAME, " v",APP_VERSION,": ",s);
}

int InitFail(string err_msg) {
   EAAlert(err_msg);
   return(INIT_FAILED);
}

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
   if (!IsConnected()) return InitFail(GetMsg(MSG_NOT_CONNECTED));
   if (!IsExpertEnabled()) return InitFail(GetMsg(MSG_EXPERT_NOT_ENABLED));
   if (!IsTradeAllowed()) return InitFail(GetMsg(MSG_TRADE_NOT_ALLOWED));
   bool res = EventSetTimer(TIMER_DELAY);
   if (IsTesting()) lastOnTimerExecution = TimeCurrent();
   
   return(INIT_SUCCEEDED);
  }
  
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
      
  }
  

bool slowdown(ENUM_TIMEFRAMES period) {
   int highest_bar = iHighest(Symbol(), period, MODE_HIGH, SlowdownLookback, 1);
   int lowest_bar = iLowest(Symbol(), period, MODE_LOW, SlowdownLookback, 1);
   double delta = iHigh(Symbol(), period, highest_bar) - iLow(Symbol(), period, lowest_bar);
   
   if (delta < (Delta * Point)) {
      return True;
   } else {
      return False;
   }
}


void OnTimer() {
   CheckTrail();
}

/* Returns the index of the SR level that the pin is on, if any. Otherwise returns -1  */

int isOnSr(ENUM_TIMEFRAMES period, double& sr_list[]) {

   double low = iLow(Symbol(), period, 1);
   double high = iHigh(Symbol(), period, 1);
   
   for (int  i = 0; i < ArraySize(sr_list); i++) {
      /* pin bar falls on an SR level */
      if (low < sr_list[i] && high > sr_list[i]) {
         return i;
      }
   }   
   return -1;
}

/* Returns the take profit based on SR if the pin is on an SR, otherwise returns -1 */
double getTpIfOnSr(ENUM_TIMEFRAMES period, double& sr_list[], pin_bar_enum pin_bar) {
   
  int i = isOnSr(period, sr_list);
  
  if (i != -1) {
   if (pin_bar == PIN_UP) {
      if (i + SR_Target >= ArraySize(sr_list)) {
         return 0;
      } else {
         return sr_list[i + SR_Target];
      }
   } else {
      if (i - SR_Target < 0) {
         return 0;
      } else {
         return sr_list[i - SR_Target];
      }
   }
  }
  
  return -1;
}
  

void SetIfPinBar(ENUM_TIMEFRAMES period, pin_bar_enum pin_bar, double& daily_sr[], double& weekly_sr[]) {

   int ticket;
   ENUM_ORDER_TYPE order_type;
   double open_price, trail, sl, risk, reward;
   double high = iHigh(Symbol(), period, 1);
   double low = iLow(Symbol() , period, 1);
   double size = high - low;
   double sl_buffer = size * (SL_Buffer_Percent / 100);
   datetime time = iTime(Symbol(), period, 1);
   
   double risk_percent = StableRisk;
   
   if (pin_bar == PIN_UP || pin_bar == PIN_DOWN) {
      double pos = low - (SHAPE_SHIFT * Point);
      ObjectCreate(PINNAME + EnumToString(pin_bar) + IntegerToString(time), OBJ_TRIANGLE, 0, Time[1], pos);
      
      double tp = getTpIfOnSr(period, daily_sr, pin_bar);
      
      if (tp != -1) {
         
         trail = (double)((size * (TrailPercent / 100)) / Point);
         if (pin_bar == PIN_UP) {
            if (tp == 0) tp = nd(low + (size * (TakeProfitPercent / 100)));    
            order_type = OP_BUY;
            sl = nd(low - sl_buffer);
            open_price = nd(low + (size * (Entry_Percent / 100)));
         } else {
            if (tp == 0) tp = nd (high - (size * (TakeProfitPercent / 100)));
            order_type = OP_SELL;
            sl = nd(high + sl_buffer);
            open_price = nd(high - (size * (Entry_Percent / 100)));
         }
         
     risk = MathAbs(open_price - sl);
     reward = MathAbs(open_price - tp);
    
     /*  */
     if (slowdown(period) && isOnSr(period, weekly_sr) != -1) {
      Alert("Exponential!");
      risk_percent = ExponentialRisk;    
      ObjectCreate("SD" + IntegerToString(Time[0]), OBJ_ARROW_THUMB_UP, 0, Time[1], high + (SHAPE_SHIFT * Point));
     }
     
     if ((reward / risk) >= AcceptableRatio)
         ticket =  PercentOrderAtPriceSend(
            Symbol(), order_type, risk_percent, open_price, Slippage, sl, tp, 
            DoubleToStr(trail), 0, TimeCurrent() + (expiration_hrs * 60 * 60)
         ); 
      }
   }
}

void DrawSR(double& sr_list[], string sr_name, int colour) {
   
   for(int i = 0; i < ArraySize(sr_list); i++) {
      string obj_name = sr_name + IntegerToString(i);
      ObjectCreate(obj_name, OBJ_HLINE, 0, Time[0], sr_list[i]);
      ObjectSetInteger(ChartID(), obj_name, OBJPROP_COLOR, colour); 
   }
}


void recalculateSR(double& daily_sr[], double& weekly_sr[]) {
        
      /* delete old SR lines and generate new ones based on params */
      DeleteAppObjects(DAILY_SR_NAME);
      DeleteAppObjects(WEEKLY_SR_NAME);
      
      findSR(PERIOD_D1, daily_sr, Daily_Tolerance, Lookback, HitThresh);
      findSR(PERIOD_W1, weekly_sr, Weekly_Tolerance, Lookback, HitThresh);
      
      DrawSR(daily_sr, DAILY_SR_NAME, clrRed);
      DrawSR(weekly_sr, WEEKLY_SR_NAME, clrBlue);
}

void OnTick()
{
   
   
   double daily_sr[], weekly_sr[];
   /* OnTimer event does not fire in the backtester so we have to test it in this hacky way */
   if (IsTesting() &&  TimeCurrent() > lastOnTimerExecution + TIMER_DELAY){
      OnTimer();
      lastOnTimerExecution = TimeCurrent();
   }
   
   if(NewBar(PERIOD_H4)) {
   
      recalculateSR(daily_sr, weekly_sr);
      
      /* If new daily formed as well, check for daily pin */
      if(TimeHour(iTime(Symbol(), PERIOD_H4, 0)) == 0) {
         if (IsInsideBar(period, 1)) {
            
         }
         SetIfPinBar(PERIOD_D1, IsPinBar(PERIOD_D1, PinBarPercent, 1), daily_sr, weekly_sr);
      } 
      
      SetIfPinBar(PERIOD_H4, IsPinBar(PERIOD_H4, PinBarPercent, 1), daily_sr, weekly_sr);
   }
   
}
//+------------------------------------------------------------------+
