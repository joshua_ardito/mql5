//+------------------------------------------------------------------+
//|                                                         Flow.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#define POST_URI "https://maker.ifttt.com/trigger/sheldon_notif/with/key/cU1eJuq7DJSDqjkVs9rxxh"

static datetime lastH4; 

enum ENUM_DIRECTION {
   DIRECTION_BUY = 0,
   DIRECTION_SELL = 1
};

input ENUM_DIRECTION direction = DIRECTION_BUY; 

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
void sendMessage(string messageText) {
  string queryParams = "&";
  char data[];
  
  string symbolStr = Symbol();
  queryParams += "value1="+symbolStr;
  queryParams += "&value2="+messageText;
  
  StringToCharArray(queryParams, data);
  char result[];
  string result_headers;
  int timeout = 0;
  // send phone call when trade is sent
  int response = WebRequest("POST", POST_URI, "", timeout, data, result, result_headers);
  Alert(response);
}

bool isFlowing(ENUM_TIMEFRAMES period) {
   
   int mode = direction == DIRECTION_BUY ? SYMBOL_ASK : SYMBOL_BID; 
   MqlTick tick;
   
   SymbolInfoTick(Symbol(), tick); 
   double price; 
   if (mode == DIRECTION_BUY) {
      price = tick.bid;
   } else {
      price = tick.ask;
   }
   
   double sma50 = iMA(Symbol(), period, 24, 0, MODE_SMA, PRICE_CLOSE);
   double sma21 = iMA(Symbol(), period, 24, 0, MODE_SMA, PRICE_CLOSE);
   double sma14 = iMA(Symbol(), period, 24, 0, MODE_SMA, PRICE_CLOSE);
   
   if (mode == DIRECTION_BUY) {
      return price > sma14 && sma14 > sma21 && sma21 > sma50;
   } else {
      return price < sma14 && sma14 < sma21 && sma21 < sma50;
   }
}

bool isInsideBar() {

   double prevHigh = iHigh(Symbol(), PERIOD_H4, 2);
   double prevLow = iLow(Symbol(), PERIOD_H4, 2);
   double currHigh = iHigh(Symbol(), PERIOD_H4, 1);
   double currLow = iLow(Symbol(), PERIOD_H4, 1);
   return prevHigh > currHigh && prevLow < currLow;
}

void sendText() {
  char data[];
  StringToCharArray("value1: test", data);
  char result[];
  string result_headers;
  int timeout = 0;
  // send phone call when trade is sent
  int response = WebRequest("POST", POST_URI, "", timeout, data, result, result_headers);
     
}

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   
   datetime currH4 = iTime(Symbol(), PERIOD_H4, 0);
   if(lastH4 != currH4){
      lastH4 = currH4;
   } else {
      return;
   }
   
   double h4Flowing = isFlowing(PERIOD_H4);
   double dailyFlowing = isFlowing(PERIOD_D1);
   
   if (!dailyFlowing || !h4Flowing) {
      return;
   }
   
   sendMessage("Flow Detected");
   
} 
//+------------------------------------------------------------------+
