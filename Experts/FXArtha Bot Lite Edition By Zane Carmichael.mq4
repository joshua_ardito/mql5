#property copyright  "© 2012 Zane Carmichael"
#property link       ""
#define APP_NAME     "FXArtha Bot Lite Edition By Zane Carmichael"
#define APP_VERSION  "1.0"

#include <stdlib.mqh>
#include <arditolib.mqh>

extern string _A = "General Settings";
extern double TakeProfitFib = 261.8;
extern double StopLossFib = 261.8;
extern double TrailStopStartFib = 150;
extern double TrailStopMovePips = 1.0;
extern double XFib = 108.0; /* # of buffer we space our entry level from IB high/low */
extern bool UseMoneyManagement = false;
extern double Risk_Percent = 2.0;
extern double FixedLotSize = 0.1;
extern string _C = "Other Settings";
extern int Slippage = 5;
extern int MagicNumber = 120130;
/* Hidden pending orders BEGIN */
extern color BuyLineColor = RoyalBlue;
extern color SellLineColor = Red;
/* Hidden pending orders END */
extern int DashboardDisplay = 2;
extern color DashboardColor = C'0x46,0x91,0xEC';
//------------------------------------------------------------
extern string s1 = "- MA1 settings";
extern int MA1_Period = 5;
extern string s2 = "---- Applied Price ----";
extern string s3 = "0 = Close";
extern string s4 = "1 = Open";
extern string s5 = "2 = High";
extern string s6 = "3 = Low";
extern string s7 = "4 = HL/2";
extern string s8 = "5 = HLC/3";
extern string s9 = "6 = HLCC/4";
extern ENUM_APPLIED_PRICE MA1_AppliedPrice = 0;
extern string sa = "---- MA Method ----";
extern string sb = "0 = Simple moving average";
extern string sc = "1 = Exponential moving average";
extern string sd = "2 = Smoothed moving average";
extern string se = "3 = Linear weighted moving average";
extern int MA1_Method = 1;
extern int MA1_Shift = 0;
//------------------------------------------------------------
extern string sf = "- MA2 settings";
extern int MA2_Period = 14;
extern int MA2_AppliedPrice = 0;
extern int MA2_Method = 1;
extern int MA2_Shift = 0;
//------------------------------------------------------------
extern string sg = "- RSI settings";
extern int RSI_Period = 14;
extern int RSI_AppliedPrice = 0;
extern double RSI_OverBuyLevel = 80;
extern double RSI_OverSellLevel = 20;
//------------------------------------------------------------

datetime last_candle_check = 1;
double Lots = 0.1;
double MyPoint;
int MyDigits;
int NumberOfRetries = 5;
bool EA_CanRun = true;
double TrailStopStartPips = 0;

/* Hidden pending orders BEGIN */
double buyPrice, sellPrice, last_Ask = 0, last_Bid = 0;
string ObjectNameBuyTrade = "buy"; string ObjectNameSellTrade = "sell";
/* Hidden pending orders END */

/* Trade counter BEGIN */
int TradeCount[11]; //[] order type: 0=B,1=S,2=BL,3=SL,4=BS,5=SS,6=Market,7=Limit,8=Stop,9=Pending,10=Total
/* Trade counter END */

double _LOTSTEP, _MINLOT, _MAXLOT, _SPREAD;
int _LOTDIGITS;
double _STOPLEVEL, _FREEZELEVEL;

double _TICKVALUE, _LOTSIZE; //used with MM functions

void EAAlert(string s) {
   Alert(APP_NAME, " v",APP_VERSION,": ",s);
}

void init() {
   EA_CanRun = true;

   if (!IsConnected()) EAAlert(MSG_NOT_CONNECTED);
   if (!IsExpertEnabled()) EAAlert(MSG_EXPERT_NOT_ENABLED);
   if (!IsTradeAllowed()) EAAlert(MSG_TRADE_NOT_ALLOWED);
   
   _LOTSTEP = MarketInfo(Symbol(), MODE_LOTSTEP);
   _MINLOT = MarketInfo(Symbol(), MODE_MINLOT);
   _MAXLOT = MarketInfo(Symbol(), MODE_MAXLOT);
   _LOTDIGITS = getLotDigits(_LOTSTEP);
   _TICKVALUE = MarketInfo(Symbol(), MODE_TICKVALUE); //used with MM functions
   _LOTSIZE = MarketInfo(Symbol(), MODE_LOTSIZE); //used with MM functions

   last_candle_check = Time[0];

   if (Point == 0.00001) MyPoint = 0.0001; 
   else if (Point == 0.001) MyPoint = 0.01; 
   else MyPoint = Point;
   
   if (Digits == 5) MyDigits = 4; 
   else if (Digits == 3) MyDigits = 2; 
   else MyDigits = Digits;
   
   if (Digits == 5 || Digits == 3) Slippage = Slippage * 10;

   Print(
      APP_NAME," v"+APP_VERSION+", started on account #", AccountNumber(),
      "; Account Name: ", AccountName(), "; Broker: ", AccountCompany() 
   );
   Print(
      "#", AccountNumber(), " Account Info: Leverage=", AccountLeverage(), 
      "; Broker server=",AccountServer(),"; LOTSTEP=",_LOTSTEP,
      "; LOTDIGITS=",_LOTDIGITS,"; MINLOT=",_MINLOT,"; MAXLOT=",_MAXLOT,
      "; TICKVALUE=",_TICKVALUE,"; LOTSIZE=",_LOTSIZE
   );

}

void deinit() {
   if (UninitializeReason() == REASON_CHARTCHANGE) return;
   Comment("");
   DeleteObjects();
}

//---------- RSI functions START ------------------------------------------
bool isRSIAboveBuyLevel(int i) {
   double p1 = iRSI(NULL, 0, RSI_Period, RSI_AppliedPrice, i);
   return(p1 >= RSI_OverBuyLevel);
}
bool isRSIBelowSellLevel(int i) {
   double p1 = iRSI(NULL, 0, RSI_Period, RSI_AppliedPrice, i);
   return(p1 <= RSI_OverSellLevel);
}
//---------- RSI functions END --------------------------------------------

int getLotDigits(double step) {
   if (step <= 0) return(0);
   double dig = 0;
   while(step < 1) {
      dig++;
      step = step * 10;
   }
   return(dig);
}

void DeleteObjects() {
   for(int i = ObjectsTotal()-1; i >= 0; i--) 
      if (StringSubstr(ObjectName(i), 0, StringLen(APP_NAME)) == APP_NAME) 
         ObjectDelete(ObjectName(i));
}

double getLotSize4(double _StopLossPips = 0) {
   double lots, risk;
   if (UseMoneyManagement && _StopLossPips > 0) { 
      risk = AccountEquity() * (Risk_Percent / 100) / _TICKVALUE;
      lots = risk / (_StopLossPips * MyPoint / Point);
   }
   else lots = FixedLotSize;

   lots = NormalizeDouble(lots / _LOTSTEP, 0) * _LOTSTEP;
   if (lots < _MINLOT) lots = _MINLOT;
   if (lots > _MAXLOT) lots = _MAXLOT;
   return(lots);
}

color getArrowColorByType(int type) { 
   if (type == OP_BUY || type == OP_BUYLIMIT || type == OP_BUYSTOP) 
      return(Blue); 
   else return(Red); 
}
void CreateErrorText(
      string Obj, string LabelCaption, 
      color LabelColor, datetime datetime1, double Price
    ) { 
      if (ObjectFind(Obj) == -1) ObjectCreate(Obj, OBJ_TEXT, 0, datetime1, Price); 
      ObjectSetText(Obj, LabelCaption, 8, "Arial Bold", LabelColor); 
    }

void CreateText(
   string Obj, string LabelCaption, color LabelColor, 
   datetime datetime1, double Price
) {
   if (ObjectFind(Obj) >= 0) ObjectDelete(Obj);
   
   for(int j = 0; j <= 1; j++) {
   for(int i = ObjectsTotal()-1; i >= 0; i--) {
      if (
         ObjectType(ObjectName(i)) == OBJ_TEXT && 
         ObjectGet(ObjectName(i), OBJPROP_TIME1) == datetime1 &&
         ObjectGet(ObjectName(i), OBJPROP_PRICE1) == Price
      ) 
         Price = Price + 20*MyPoint;
   }
   }
   ObjectCreate(Obj, OBJ_TEXT, 0, datetime1, Price);
   ObjectSetText(Obj, LabelCaption, 10, "Arial Bold", LabelColor);
}

int OpenOrder_v3(
      string symbol, int _OrderType, double LotSize, int slippage, 
      double StopLoss, double TakeProfit, string Comments, int magic, 
      int stop_type = 1, color ArrowColor = -2
   ) {
   //OpenOrder_v3(Symbol(), OP_SELL, Lots, Slippage, StopLossPips, 
   // TakeProfitPips, "", MagicNumber, 1, Blue);
   
   if (_IsTradeAllowed() < 0) return(-1);

   if (_OrderType > 1) return(0);

   double _EntryPrice;
   int ticket, j = 0, err = 0, __STOPLEVEL;
   if (symbol == "") symbol = Symbol();

   RefreshRates();
   __STOPLEVEL = MarketInfo(_Symbol, MODE_STOPLEVEL);
   double _Bid = MarketInfo(_Symbol, MODE_BID);
   double _Ask = MarketInfo(_Symbol, MODE_ASK);
   int digits = MarketInfo(_Symbol, MODE_DIGITS);
   int err_type;
   double _MyPoint;
   
   if (_Point == 0.00001) _MyPoint = 0.0001; 
   else if (_Point == 0.001) _MyPoint = 0.01; 
   else _MyPoint = _Point;

   if (_OrderType == OP_SELL) _EntryPrice = _Bid; 
   else if (_OrderType == OP_BUY) _EntryPrice = _Ask; 
   else return(0);

   ticket = OrderSend(
      _Symbol, _OrderType, LotSize, _EntryPrice, Slippage, 0, 0, 
      Comments, magic, 0, ArrowColor
   );
   
   if (ticket <= 0) {
      err = GetLastError();
      err_type = GetErrorType(err);
      Print("OrderSend error (", err, "): ", ErrorDescription(err), "; OrderType: ",
             _OrderType, "; LotSize: ", DoubleToStr(LotSize, _LOTDIGITS),
             "; Open Price: " + DoubleToStr(_EntryPrice, _Digits), 
             "; Ask: "+DoubleToStr(_Ask, _Digits) + "; Bid: ", 
             DoubleToStr(_Bid, _Digits) + "; STOPLEVEL: ", 
             DoubleToStr((__STOPLEVEL*_Point)/_MyPoint, 1),";"
            );
      
      if (err > 1) if (_Symbol == Symbol()) 
         CreateErrorText(
            "error_open_trade_"+Time[0], "oe" + err, ArrowColor, Time[0], Low[0]-2*_MyPoint
         );
      if (err_type == ERRTYPE_FATAL) {
         EA_CanRun = false; 
         return(0); 
      }
      if (err_type != ERRTYPE_RETRY && err_type != ERRTYPE_WAIT_BEFORE_RETRY) return(0);
         
      if (err_type == ERRTYPE_WAIT_BEFORE_RETRY) {
         Print(
            "EA must wait a few seconds before attempting to repeat the",
            "last trading action to obey MQL rules"
         );
         Sleep(5000);
      } //if (err...
   } 
   j = 1;
   while ( (ticket <= 0) && (j <= NumberOfRetries) ) {
      Sleep(1000);
      j++;
      Print("OrderSend retry #" + j);

      RefreshRates();
      __STOPLEVEL = MarketInfo(_Symbol, MODE_STOPLEVEL);
      _Bid = MarketInfo(_Symbol, MODE_BID);
      _Ask = MarketInfo(_Symbol, MODE_ASK);
      if (_OrderType == OP_SELL) _EntryPrice = _Bid; 
      else if (_OrderType == OP_BUY) _EntryPrice = _Ask; 
      else return(0);

      ticket = OrderSend(
         Symbol(), _OrderType, LotSize, _EntryPrice, Slippage, 
         0, 0, Comments, magic, 0, ArrowColor
      );
      
      if (ticket <= 0) {
         err = GetLastError();
         Print(
            "OrderSend error (", err, "): ",ErrorDescription(err),"; OrderType: ",
            _OrderType, "; LotSize: ", DoubleToStr(LotSize, _LOTDIGITS), "; Open Price: ", 
            DoubleToStr(_EntryPrice, _Digits), "; Ask: ", DoubleToStr(_Ask, _Digits), 
            "; Bid: ", DoubleToStr(_Bid, _Digits), "; STOPLEVEL: ", 
            DoubleToStr((__STOPLEVEL*_Point)/_MyPoint, 1),";"
         );
         if (err_type != ERRTYPE_RETRY && err_type != ERRTYPE_WAIT_BEFORE_RETRY) 
         return(0);
         if (j >= NumberOfRetries) break;
         if (err_type == ERRTYPE_WAIT_BEFORE_RETRY) {
            Print("EA must wait a few seconds before attempting to repeat the last trading action to obey MQL rules");
            Sleep(5000);
         } //if (err...
      } //if (ticket <= 0)
   } //while

   if (IsTesting() && ticket > 0 && _Symbol == Symbol()) 
         CreateText(APP_NAME+"_new_trade_"+ticket, ticket, Red, Time[0], _Bid+20*_MyPoint);

   if ( (StopLoss > 0) || (TakeProfit > 0) )
   if (ticket > 0) {
      if (OrderSelect(ticket, SELECT_BY_TICKET) == true) {
         if (stop_type == 1)
         if (_OrderType == OP_BUY) {
            if (StopLoss > 0) StopLoss = (OrderOpenPrice()-StopLoss*_MyPoint);
            if (TakeProfit > 0) TakeProfit = (OrderOpenPrice()+TakeProfit*_MyPoint);
         } else if (_OrderType == OP_SELL) {
            if (StopLoss > 0) StopLoss = (OrderOpenPrice()+StopLoss*_MyPoint);
            if (TakeProfit > 0) TakeProfit = (OrderOpenPrice()-TakeProfit*_MyPoint);
         } else return(ticket);

         RefreshRates();
         _Bid = MarketInfo(_Symbol, MODE_BID);
         _Ask = MarketInfo(_Symbol, MODE_ASK);

         AdjustStopLoss_v3(OrderType(), OrderTicket(), OrderSymbol(), OrderOpenPrice(), StopLoss, _Ask, _Bid, __STOPLEVEL);
         AdjustTakeProfit_v3(OrderType(), OrderTicket(), OrderSymbol(), OrderOpenPrice(), TakeProfit, _Ask, _Bid, __STOPLEVEL);

         j = 0;
         while (!OrderModify(OrderTicket(), OrderOpenPrice(), NormalizeDouble(StopLoss, _Digits), NormalizeDouble(TakeProfit, _Digits), 0, ArrowColor) && (j <= NumberOfRetries) ) {
            err = GetLastError();
            Print("OrderModify error ("+err+"): "+ErrorDescription(err) + "; OrderTicket: "+OrderTicket()+"; OrderType: "+OrderType()+"; LotSize: "+DoubleToStr(LotSize, _LOTDIGITS)+"; Open Price: " + DoubleToStr(OrderOpenPrice(), _Digits) + "; StopLoss: "+DoubleToStr(StopLoss, _Digits)+"; TakeProfit: "+DoubleToStr(TakeProfit, _Digits)+"; Ask: "+DoubleToStr(_Ask, _Digits)+"; Bid: "+DoubleToStr(_Bid, _Digits)+"; STOPLEVEL: "+DoubleToStr((__STOPLEVEL*_Point)/_MyPoint, 1)+";");
            if (err != 0 && err != 4 && err != 6 && err != 128 && err != 129 && err != 130 && err != 135 && err != 136 && err != 137 && err != 138 && err != 145 && err != 146) break;
            if (j >= NumberOfRetries) break;
            Sleep(1000);
            if (err_type == ERRTYPE_WAIT_BEFORE_RETRY) {
               Print("EA must wait a few seconds before attempting to repeat the last trading action to obey MQL rules");
               Sleep(5000);
            } //if (err...
            j++;
            Print("OrderModify retry #" + j);
            RefreshRates();
            _Bid = MarketInfo(_Symbol, MODE_BID);
            _Ask = MarketInfo(_Symbol, MODE_ASK);
            AdjustStopLoss_v3(OrderType(), OrderTicket(), OrderSymbol(), OrderOpenPrice(), StopLoss, _Ask, _Bid, __STOPLEVEL);
            AdjustTakeProfit_v3(OrderType(), OrderTicket(), OrderSymbol(), OrderOpenPrice(), TakeProfit, _Ask, _Bid, __STOPLEVEL);
         } //while
         if (err > 1) if (_Symbol == Symbol()) CreateErrorText("error_modify_trade_"+Time[0], "me"+err, ArrowColor, Time[0], Low[0]-2*_MyPoint);
      } else Print("ERROR: "+ErrorDescription(GetLastError()) );
   }
   return(ticket);
}

bool AdjustStopLoss_v3(
   int _OrderType, int _OrderTicket, string symbol, 
   double _EntryPrice, double &_StopLoss, 
   double _Ask, double _Bid, int &__STOPLEVEL
) {
   //_EntryPrice should be zero for market orders
   if (_StopLoss <= 0) return(false);
   string price_type_text = "market";
   int _DISTANCE = 0, _DIFF;
   int digits = MarketInfo(symbol,MODE_DIGITS);
   double point = MarketInfo(symbol,MODE_POINT), _MyPoint;
   if (point == 0.00001) _MyPoint = 0.0001; 
   else if (point == 0.001) _MyPoint = 0.01; 
   else _MyPoint = point;
   
   double StopLossOld = _StopLoss;

   __STOPLEVEL = MarketInfo(symbol, MODE_STOPLEVEL);
   switch(_OrderType) {
      case OP_BUY:
         _DISTANCE = (_Bid-_StopLoss)/point;
         break;
      case OP_SELL: 
         _DISTANCE = (_StopLoss-_Ask)/point;
         break;
      case OP_BUYSTOP:
         _DISTANCE = (_EntryPrice-_StopLoss) / point;
         break;
      case OP_BUYLIMIT:
         _DISTANCE = (_EntryPrice-_StopLoss) / point;
         break;
      case OP_SELLSTOP:
         _DISTANCE = (_StopLoss-_EntryPrice)/ point;
         break;
      case OP_SELLLIMIT: 
         _DISTANCE = (_StopLoss-_EntryPrice)/ point;
         break;
    }
    if (_DISTANCE < __STOPLEVEL) {
      _DIFF = __STOPLEVEL - _DISTANCE;
      if (_DIFF > 0) { //price too close to market/open price (if distance too small)
         switch(_OrderType) {
            case OP_BUY:
               _StopLoss = NormalizeDouble(_Bid-__STOPLEVEL*point, digits);
            case OP_SELL:
               _StopLoss = NormalizeDouble(_Ask+__STOPLEVEL*point, digits);
            case OP_BUYSTOP:
               _StopLoss = NormalizeDouble(_EntryPrice-__STOPLEVEL*point, digits);
            case OP_BUYLIMIT:
               _StopLoss = NormalizeDouble(_EntryPrice-__STOPLEVEL*point, digits);
            case OP_SELLSTOP:
               _StopLoss = NormalizeDouble(_EntryPrice+__STOPLEVEL*point, digits);
            case OP_SELLLIMIT:
               _StopLoss = NormalizeDouble(_EntryPrice+__STOPLEVEL*point, digits);
         }
         if (
            _OrderType == OP_BUYLIMIT || _OrderType == OP_BUYSTOP || 
            _OrderType == OP_SELLLIMIT || _OrderType == OP_SELLSTOP
         ) price_type_text = "open";
         
         if (_OrderType == OP_BUY) price_type_text = "market BID";
         if (_OrderType == OP_SELL) price_type_text = "market ASK";
         
         Print(
            TradeTypeText(_OrderType), " #", _OrderTicket, " stop loss adjusted by ", 
            DoubleToStr((_DIFF*_Point)/_MyPoint, 1)+" pips because it was too close to ", 
            price_type_text, " price (Ask: ", DoubleToStr(_Ask, _Digits), 
            "; Bid: ", DoubleToStr(_Bid, _Digits), "; Distance: ", 
            DoubleToStr((_DISTANCE*_Point)/_MyPoint, 1), " pips)"
         );
            
         Print(
            TradeTypeText(_OrderType), " #", _OrderTicket, " New stop loss ", 
            DoubleToStr(_StopLoss, _Digits), "; Old stop loss ", 
            DoubleToStr(StopLossOld, _Digits), "; Minimum distance between stop loss and ", 
            price_type_text, " price (StopLevel) must be at least ", 
            DoubleToStr((__STOPLEVEL*_Point)/_MyPoint, 1)+" pips or the size of the spread"
         );
            
         return(true);
      } //if (_DIFF > 0)
   } else return(false); 
   return(false);
}

bool AdjustTakeProfit_v3(
   int _OrderType, int _OrderTicket, string symbol, double _EntryPrice, 
   double &_TakeProfit, double _Ask, double _Bid, int &__STOPLEVEL
) {
   //_EntryPrice should be zero for market orders
   if (_TakeProfit <= 0) return(false);
   string price_type_text = "market";
   int _DISTANCE = 0, _DIFF;
   double _MyPoint;
   if (_Point == 0.00001) _MyPoint = 0.0001; else if (_Point == 0.001) _MyPoint = 0.01; else _MyPoint = _Point;
   double TakeProfitOld = _TakeProfit;

   __STOPLEVEL = MarketInfo(symbol, MODE_STOPLEVEL);
      if (_OrderType == OP_BUY) _DISTANCE = (_TakeProfit-_Bid)/_Point;
      if (_OrderType == OP_SELL) _DISTANCE = (_Ask-_TakeProfit)/_Point;
      if (_OrderType == OP_BUYSTOP) _DISTANCE = (_TakeProfit-_EntryPrice)/_Point;
      if (_OrderType == OP_BUYLIMIT) _DISTANCE = (_TakeProfit-_EntryPrice)/_Point;
      if (_OrderType == OP_SELLSTOP) _DISTANCE = (_EntryPrice-_TakeProfit)/_Point;
      if (_OrderType == OP_SELLLIMIT) _DISTANCE = (_EntryPrice-_TakeProfit)/_Point;
      if (_DISTANCE < __STOPLEVEL) {
         _DIFF = __STOPLEVEL - _DISTANCE;
         if (_DIFF > 0) { //price too close to market/open price (if distance too small)
            if (_OrderType == OP_BUY) 
               _TakeProfit = NormalizeDouble(_Bid+__STOPLEVEL*_Point, _Digits);
            if (_OrderType == OP_SELL) 
               _TakeProfit = NormalizeDouble(_Ask-__STOPLEVEL*_Point, _Digits);
            if (_OrderType == OP_BUYSTOP) 
               _TakeProfit = NormalizeDouble(_EntryPrice+__STOPLEVEL*_Point, _Digits);
            if (_OrderType == OP_BUYLIMIT) 
               _TakeProfit = NormalizeDouble(_EntryPrice+__STOPLEVEL*_Point, _Digits);
            if (_OrderType == OP_SELLSTOP) 
               _TakeProfit = NormalizeDouble(_EntryPrice-__STOPLEVEL*_Point, _Digits);
            if (_OrderType == OP_SELLLIMIT) 
               _TakeProfit = NormalizeDouble(_EntryPrice-__STOPLEVEL*_Point, _Digits);
            if (
               _OrderType == OP_BUYLIMIT || _OrderType == OP_BUYSTOP || 
               _OrderType == OP_SELLLIMIT || _OrderType == OP_SELLSTOP
            ) price_type_text = "open";
            
            if (_OrderType == OP_BUY) price_type_text = "market BID";
            if (_OrderType == OP_SELL) price_type_text = "market ASK";
            
            Print(TradeTypeText(_OrderType)+" #"+_OrderTicket+" take profit adjusted by "+DoubleToStr((_DIFF*_Point)/_MyPoint, 1)+" pips because it was too close to "+price_type_text+" price (Ask: "+DoubleToStr(_Ask, _Digits)+"; Bid: "+DoubleToStr(_Bid, _Digits)+"; Distance: "+DoubleToStr((_DISTANCE*_Point)/_MyPoint, 1)+" pips)");
            Print(TradeTypeText(_OrderType)+" #"+_OrderTicket+" New take profit "+DoubleToStr(_TakeProfit, _Digits)+"; Old take profit "+DoubleToStr(TakeProfitOld, _Digits)+"; Minimum distance between take profit and "+price_type_text+" price (StopLevel) must be at least "+DoubleToStr((__STOPLEVEL*_Point)/_MyPoint, 1)+" pips or the size of the spread");
            return(true);
         } //if (_DIFF > 0)
      } else return(false); //if (_DISTANCE < __STOPLEVEL)
   return(false);
}

int _IsTradeAllowed(uint MaxWaiting_sec = 30) {
   // check whether the trade context is free
   if (!IsTradeAllowed()) {
      uint StartWaitingTime = GetTickCount();
      Print(GetMsg(MSG_CONTEXT_BUSY));
      // infinite loop
      while(true) {
         // if the expert was terminated by the user, stop operation
         if (IsStopped()) { 
            Print(GetMsg(MSG_EXPERT_TERMINATED)); 
            return(-1); 
         } 
         // if the waiting time exceeds the time specified in the 
         // MaxWaiting_sec variable, stop operation, as well
         if (GetTickCount() - StartWaitingTime > MaxWaiting_sec * 1000) {
            Print("The waiting limit exceeded (" + MaxWaiting_sec + ")! EA may fail to perform trading actions because broker trade context is busy!");
            return(-2);
         } //if (GetTickCount()
         // if the trade context has become free,
         if (IsTradeAllowed()) {
            Print(GetMsg(MSG_CONTEXT_FREE));
            return(0);
         } //if (IsTradeAllowed())
         // if no loop breaking condition has been met, "wait" for 0.1 
         // second and then restart checking            Sleep(100);
      } //while
   } else { //if (!IsTradeAllowed())
      //Print("Trade context is free!");
      return(1);
   } //else if (!IsTradeAllowed())
}

void countTrades2() {
   double sl, dline, p;
   ArrayInitialize(TradeCount, 0);

   for(int i = OrdersTotal()-1; i >= 0; i--) {
      if (OrderSelect(i, SELECT_BY_POS, MODE_TRADES) == false) continue;
      if (OrderSymbol() != Symbol() ) continue;
      if (OrderMagicNumber() != MagicNumber) continue;

 /*TODO: Figure out wtf these magic numbers are */
      TradeCount[OrderType()]++;
      if (OrderType() == OP_BUY || OrderType() == OP_SELL) 
         TradeCount[6]++;
      if (OrderType() == OP_BUYLIMIT || OrderType() == OP_SELLLIMIT) { 
         TradeCount[7]++; 
         TradeCount[9]++; 
      }
      if (OrderType() == OP_BUYSTOP || OrderType() == OP_SELLSTOP) { 
         TradeCount[8]++; 
         TradeCount[9]++; 
      }
      TradeCount[10]++;

/*TrailingStop/BreakEven: Begin*/
      if (TrailStopStartPips > 0) {
         dline = OrderOpenPrice();
         sl = OrderStopLoss();
         if (OrderType() == OP_BUY) {
            if (
               OrderStopLoss() > 0 && 
               nd(OrderStopLoss(), Digits) > nd(OrderOpenPrice(), Digits) 
            ) dline = OrderStopLoss();
            
            p = (OrderClosePrice() - dline)/MyPoint;
            if (p > 0 && p >= TrailStopStartPips) 
               sl = nd(OrderClosePrice() - (TrailStopStartPips - TrailStopMovePips)*MyPoint, Digits);
         } else if (OrderType() == OP_SELL) {
            if (OrderStopLoss() > 0 && nd(OrderStopLoss(), Digits) < nd(OrderOpenPrice(), Digits) ) dline = OrderStopLoss();
            p = (dline - OrderClosePrice())/MyPoint;
            if (p > 0 && p >= TrailStopStartPips) sl = nd(OrderClosePrice() + (TrailStopStartPips - TrailStopMovePips)*MyPoint, Digits);
         } //else if (OrderType() == OP_SELL)
         if (nd(sl, Digits) != nd(OrderStopLoss(), Digits) ) 
            OrderModify(OrderTicket(), OrderOpenPrice(), sl, OrderTakeProfit(), 0, CLR_NONE);
      } 
   } 
}

/* Hidden pending orders BEGIN */
void CreateHLine(string Obj, color LineColor, double Price) {
   if (ObjectFind(Obj) == -1) ObjectCreate(Obj, OBJ_HLINE, 0, 0, Price);
   ObjectSet(Obj, OBJPROP_PRICE1, Price);
   ObjectSet(Obj, OBJPROP_COLOR, LineColor);
   ObjectSet(Obj, OBJPROP_WIDTH, 1);
   ObjectSet(Obj, OBJPROP_BACK, true);
}
double getHLineValue(string LineName) {
   int otype = ObjectType(LineName);
   if (otype == OBJ_HLINE) return(ObjectGet(LineName, OBJPROP_PRICE1)); else return(0);
}
/* Hidden pending orders END */

bool IsMA1AboveMA2Up(int i, int tf = 0) {
   double p1_1 = iMA(NULL, tf, MA1_Period, MA1_Shift, MA1_Method, MA1_AppliedPrice, i);
   double p2_1 = iMA(NULL, tf, MA2_Period, MA2_Shift, MA2_Method, MA2_AppliedPrice, i);
   return(p1_1 > p2_1);
}

bool IsMA1BelowMA2Up(int i, int tf = 0) {
   double p1_1 = iMA(NULL, tf, MA1_Period, MA1_Shift, MA1_Method, MA1_AppliedPrice, i);
   double p2_1 = iMA(NULL, tf, MA2_Period, MA2_Shift, MA2_Method, MA2_AppliedPrice, i);
   return(p1_1 < p2_1);
}

void CreateBlock99(color LabelColor, int font_size, int block_count, int step_pixels) {
   string Obj;
   for (int i = 0; i < block_count; i++) {
      Obj = APP_NAME+"block99-"+(i+1); 
      if (ObjectFind(Obj) == -1) ObjectCreate(Obj, OBJ_LABEL, 0, 0, 0); 
      ObjectSetText(Obj, "g", font_size, "Webdings", LabelColor);
      ObjectSet(Obj, OBJPROP_XDISTANCE, 85); 
      ObjectSet(Obj, OBJPROP_YDISTANCE, 72+step_pixels*i); 
      ObjectSet(Obj, OBJPROP_COLOR, LabelColor); 
      ObjectSet(Obj, OBJPROP_FONTSIZE, font_size);
   } 
}

string extraSpaces(int c = 0) {
   string s = "";
   for(int i = 0; i < c; i++) s = s + " ";
   return(s);
}

void printComments() {
   if (DashboardDisplay <= 0) return;

   if (DashboardDisplay > 1) {
      CreateBlock99(DashboardColor, 140, 1, 130);
   } //if (DashboardDisplay > 1)
   string _spaces = extraSpaces(30);
   string s = "\n\n\n\n\n\n";
   s = s + _spaces+APP_NAME+" v"+APP_VERSION+"\n\n";
   s = s + _spaces+"Spread: "+DoubleToStr((_SPREAD*Point)/MyPoint, 1)+" pips\n";
   s = s + _spaces+"Stops Level: "+DoubleToStr((_STOPLEVEL*Point)/MyPoint, 1)+" pips\n";
   s = s + _spaces+"Lot Size: ";
   if (UseMoneyManagement) {
      s = s + "Variable\n";
      s = s + _spaces+"Lot size based on "+DoubleToStr(Risk_Percent, 2)+"% equity risk\n";
   } else s = s +DoubleToStr(Lots, _LOTDIGITS)+"\n";
   s = s + _spaces+"Take Profit: "+DoubleToStr(TakeProfitFib, 1)+"% Fib\n";
   s = s + _spaces+"Stop Loss: "+DoubleToStr(StopLossFib, 1)+"% Fib\n";
   s = s + _spaces+"Trailing Stop: "+DoubleToStr(TrailStopStartFib, 1)+"% Fib\n";
   s = s + _spaces+"Magic Number: "+MagicNumber+"\n";
   s = s + _spaces+"Buy/Sell/Total: "+TradeCount[OP_BUY]+"/"+TradeCount[OP_SELL]+"/"+TradeCount[10]+"\n";
   if (buyPrice > 0) s = s + _spaces+"Buy @ "+DoubleToStr(buyPrice, Digits)+"\n";
   if (sellPrice > 0) s = s + _spaces+"Sell @ "+DoubleToStr(sellPrice, Digits)+"\n";

   Comment(s);
}

void start() {
   if (!EA_CanRun) return;

/* Hidden pending orders BEGIN */
   if (last_Ask == 0) last_Ask = Ask;
   if (last_Bid == 0) last_Bid = Bid;
/* Hidden pending orders END */

   double StopLossPips, TakeProfitPips, ib_size, p;
   int t;

   _SPREAD = MarketInfo(Symbol(), MODE_SPREAD);
   _STOPLEVEL = MarketInfo(Symbol(), MODE_STOPLEVEL);

   countTrades2();

   if (last_candle_check != Time[0]) {

      last_candle_check = Time[0];

      if (IsInsideBar(1)) {
/* Hidden pending orders BEGIN */
         ib_size = (High[1] - Low[1])/MyPoint;
         GlobalVariableSet("ib_size", ib_size);
         p = (High[1] - Low[1]) * MathAbs((XFib-100)/100);
         if (!isRSIAboveBuyLevel(1)) 
            CreateHLine(ObjectNameBuyTrade, BuyLineColor, High[1]+p);
         if (!isRSIBelowSellLevel(1)) 
            CreateHLine(ObjectNameSellTrade, SellLineColor, Low[1]-p);
         if (Ask >= High[1]) last_Ask = 0;
         if (Bid <= Low[1]) last_Bid = 0;
/* Hidden pending orders END */
      } //if (IsInsideBar(1))

   } //if (last_candle_check != Time[0])

   buyPrice  = getHLineValue(ObjectNameBuyTrade);
   sellPrice = getHLineValue(ObjectNameSellTrade);

   if ( 
      (buyPrice > 0  && ( (Ask >= buyPrice  && last_Ask < buyPrice ) || 
      (Ask <= buyPrice  && last_Ask > buyPrice ) ) ) 
   )
   if (IsMA1AboveMA2Up(0) || isRSIBelowSellLevel(0) ) {
      if (GlobalVariableCheck("ib_size")) 
         ib_size = GlobalVariableGet("ib_size"); 
      else ib_size = (High[1] - Low[1])/MyPoint;
      
      StopLossPips = ib_size * MathAbs((StopLossFib-100)/100);
      TakeProfitPips = ib_size * MathAbs((TakeProfitFib-100)/100);
      TrailStopStartPips = ib_size * MathAbs((TrailStopStartFib-100)/100);
      Lots = getLotSize4(StopLossPips);
      
      t = OpenOrder_v3(
         Symbol(), OP_BUY, Lots, Slippage, StopLossPips, 
         TakeProfitPips, "", MagicNumber, 1, Blue
      );
      
      if (t > 0) ObjectDelete(ObjectNameBuyTrade);
   } else ObjectDelete(ObjectNameBuyTrade);

   if ( (sellPrice > 0 && ( (Bid <= sellPrice && last_Bid > sellPrice) || 
      (Bid >= sellPrice && last_Bid < sellPrice) ) ) 
   )
   if (IsMA1BelowMA2Up(0) || isRSIAboveBuyLevel(0) ) {
      if (GlobalVariableCheck("ib_size")) 
         ib_size = GlobalVariableGet("ib_size"); 
      else ib_size = (High[1] - Low[1])/MyPoint;
      StopLossPips = ib_size * MathAbs((StopLossFib-100)/100);
      TakeProfitPips = ib_size * MathAbs((TakeProfitFib-100)/100);
      TrailStopStartPips = ib_size * MathAbs((TrailStopStartFib-100)/100);
      Lots = getLotSize4(StopLossPips);
      t = OpenOrder_v3(
         Symbol(), OP_SELL, Lots, Slippage, StopLossPips, 
         TakeProfitPips, "", MagicNumber, 1, Red
      );
      
      if (t > 0) ObjectDelete(ObjectNameSellTrade);
   } else ObjectDelete(ObjectNameSellTrade);

   last_Ask = Ask;
   last_Bid = Bid;
/* Hidden pending orders END */

   printComments();

}

