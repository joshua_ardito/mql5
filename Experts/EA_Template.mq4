//+------------------------------------------------------------------+
//|                                               Pin Bar Trader.mq4 |
//|                                    Copyright 2017, Joshua Ardito |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Joshua Ardito"
#property link      ""
#property version   "1.00"
#property strict

#define APP_NAME     "EA NAME GOES HERE"
#define APP_VERSION  "1.0"
#define SECONDS_IN_DAY 86400
#define TRAIL_THRESH 20

/* Amount in seconds that the OnTimer() event will take to fire */
#define TIMER_DELAY 600

#include <stdlib.mqh>
#include <arditolib.mqh>

/* Used to simulate OnTimer firing in testing */
datetime lastOnTimerExecution;


void EAAlert(string s) {
   Alert(APP_NAME, " v",APP_VERSION,": ",s);
}

int InitFail(string err_msg) {
   EAAlert(err_msg);
   return(INIT_FAILED);
}

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
   if (!IsConnected()) return InitFail(GetMsg(MSG_NOT_CONNECTED));
   if (!IsExpertEnabled()) return InitFail(GetMsg(MSG_EXPERT_NOT_ENABLED));
   if (!IsTradeAllowed()) return InitFail(GetMsg(MSG_TRADE_NOT_ALLOWED));
   bool res = EventSetTimer(TIMER_DELAY);
   if (IsTesting()) lastOnTimerExecution = TimeCurrent();
   Alert("Timer initialized!");
   return(INIT_SUCCEEDED);
  }
  
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
      
  }
  
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
{
   /* Use this if your EA saves trailing stops in the trade comments */
   /* CheckTrail(); */  
}
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
      /* OnTimer event does not fire in the backtester so we have to test it in this hacky way */
   if (IsTesting() &&  TimeCurrent() > lastOnTimerExecution + TIMER_DELAY){
      OnTimer();
      lastOnTimerExecution = TimeCurrent();
   }
  }
