//+------------------------------------------------------------------+
//|                                               PinBarExpert.mq4   |
//|                                    Copyright 2017, Joshua Ardito |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Joshua Ardito"
#property link      ""
#property version   "1.00"
#property strict

#define APP_NAME     "IB Trader"
#define APP_VERSION  "1.0"
#define SECONDS_IN_DAY 86400
#define TRAIL_THRESH 20

#define DAILY_SR_NAME "DAILY"
#define WEEKLY_SR_NAME "WEEKLY"
#define PINNAME "pinbar"

/* Amount in seconds that the OnTimer() event will take to fire */
#define TIMER_DELAY 600
#include <stdlib.mqh>
#include <arditolib.mqh>
#include <PinBarTrader.mqh>
#include <SRLevels.mqh>

/* Used to simulate OnTimer firing in testing */
datetime lastOnTimerExecution;
bool dailyPinExists = false;

SRLevels *dailySRLevels;
SRLevels *weeklySRLevels;

PinBarTrader *pinBarTrader;

/* Slowdown Settings */
extern const int SlowdownThreshold = 2100;
extern const int SlowdownLookback = 2;

extern const double PinBarPercent = 0.65;
extern const double TrailPercent = 110;

/* Money Management settings */
extern const double ExponentialRisk = 30;
extern const double StableRisk = 5;
extern const double AcceptableRatio = 1;

/* Order Settings */
extern const double TPPercent = 261;
extern const int SRTarget = 2;
extern const int FibTarget = 161;
extern const int Slippage = 3;
extern const double EntryPercent = 70;
extern const double SLBufferPercent = 15;
extern const int ExpirationHrs = 24;
/* SR Detection Settings */

/* Color Settings */

extern const color WeeklySRColor = clrRed;
extern const color DailySRColor = clrBlue;

extern const int WeeklyTolerance = 1500;
extern const int DailyTolerance = 600;
extern const int Lookback = 500;
extern const int HitThresh = 15;

extern const bool TradeH4 = false;

void EAAlert(string s) {
   Alert(APP_NAME, " v",APP_VERSION,": ",s);
}
int InitFail(string err_msg) {
   EAAlert(err_msg);
   return(INIT_FAILED);
}

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
   if (!IsConnected()) return InitFail(GetMsg(MSG_NOT_CONNECTED));
   if (!IsExpertEnabled()) return InitFail(GetMsg(MSG_EXPERT_NOT_ENABLED));
   if (!IsTradeAllowed()) return InitFail(GetMsg(MSG_TRADE_NOT_ALLOWED));
   bool res = EventSetTimer(TIMER_DELAY);
   if (IsTesting()) lastOnTimerExecution = TimeCurrent();
   
   dailySRLevels = new SRLevels(
      PERIOD_D1, DailySRColor, DailyTolerance, Lookback, HitThresh, DAILY_SR_NAME
   );
   
   weeklySRLevels = new SRLevels(
      PERIOD_W1, WeeklySRColor, WeeklyTolerance, Lookback, HitThresh, WEEKLY_SR_NAME
   );
   
   pinBarTrader = new PinBarTrader(
      PinBarPercent, StableRisk, ExponentialRisk, SLBufferPercent, 
      TPPercent, EntryPercent, TrailPercent, SlowdownThreshold, SlowdownLookback, SRTarget, 
      AcceptableRatio, Slippage, ExpirationHrs);
   return(INIT_SUCCEEDED);
  }
  
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
   // TODO: right now I don't think dailySRLevels is getting used at all
   delete dailySRLevels;
   delete weeklySRLevels;
   delete pinBarTrader;
  }

void OnTimer() {
   CheckTrail();
}

void OnTick()
{
   
   
   double daily_sr[], weekly_sr[];
   /* OnTimer event does not fire in the backtester so we have to test it in this hacky way */
   if (IsTesting() &&  TimeCurrent() > lastOnTimerExecution + TIMER_DELAY){
      OnTimer();
      lastOnTimerExecution = TimeCurrent();
   }
   
   if(NewBar(PERIOD_H4)) {
   
      dailySRLevels.calculateSR(daily_sr);
      weeklySRLevels.calculateSR(weekly_sr);
      
      /* If new daily formed as well, check for daily pin and IB */
      if(TimeHour(iTime(Symbol(), PERIOD_H4, 0)) == 0) {
         dailyPinExists = pinBarTrader.setIfPinBar( PERIOD_D1, daily_sr, weekly_sr);
      } 
      
      /* TODO: Make sure this doesn't trade on breakout */ 
      if (TradeH4 && dailyPinExists) {
         pinBarTrader.setIfPinBar(PERIOD_H4,daily_sr, weekly_sr);
      }
   }
   
}
//+------------------------------------------------------------------+
