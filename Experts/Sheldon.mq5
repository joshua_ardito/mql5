//+------------------------------------------------------------------+
//|                                               Sheldon.mq4        |
//|                                    Copyright 2017, Joshua Ardito |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Joshua Ardito"
#property link      ""
#property version   "1.00"
#property strict

#define APP_NAME     "Sheldon"
#define APP_VERSION  "1.0"
#define DAILY_SR_NAME "DAILY"
#define WEEKLY_SR_NAME "WEEKLY"

/* Amount in seconds that the OnTimer() event will take to fire */
#define TIMER_DELAY 600

#include <MTMore/SRLevels.mqh>
#include <Sheldon/PinBarTrader.mqh>
#include <Sheldon/IBTrader.mqh>
#include <MTMore/OrderManager.mqh>
#include <ChartContext.mqh> 
#include <Charts/Chart.mqh>


/* General Properties */ 
extern const double PinBarPercent = 0.65;
extern const bool TradeH4 = false;

/* Slowdown Properties */
extern const int SlowdownLookback = 2;

extern const int DailySlowdownThreshold = 2100;
extern const int H4SlowdownThreshold = 400;

extern const int DailyIBTrendThreshold = 100;
extern const int H4IBTrendThreshold = 200; 

/* Money Management Properties */
extern const double ExponentialRisk = 30;
extern const double StableRisk = 5;
extern const bool TradeStable = true;
extern const double AcceptableRatio = 1;


/* IB Trend Determination Properties */
extern const bool SetOnBothSides = false;
extern const bool UseMA = true;

extern const ENUM_APPLIED_PRICE MA1AppliedPrice = 0;
extern const ENUM_MA_METHOD MA1Method = 0;
extern const int MA1Period = 5;

extern const ENUM_APPLIED_PRICE MA2AppliedPrice = PRICE_CLOSE;
extern const ENUM_MA_METHOD MA2Method = 0;
extern const int MA2Period = 14;

/* Pin Retracement Properties */
extern const int PinRetracementLookback = 2;

extern const int DailyPinRetracementLowerBound = 400;
extern const int H4PinRetracementLowerBound = 200;

extern const int DailyPinRetracementUpperBound = 2000; 
extern const int H4PinRetracementUpperBound = 100; 


/* Order Settings */

extern const double PinEntryPercent = 70;
extern const double IBEntryPercent = 115;

extern const double DailySLBufferPercent = 15;
extern const double H4SLBufferPercent = 15;

extern const double DailyIBTakeProfitPercent = 261;
extern const double H4IBTakeProfitPercent = 261;
extern const double DailyPinTakeProfitPercent = 261;
extern const double H4PinTakeProfitPercent = 261;

/* Order Metadata Settings */
extern const int Slippage = 3;
extern const int DailyExpirationHrs = 24;
extern const int H4ExpirationHrs = 8;

/* SR Target Settings */
extern const int SRTarget1 = 1;
extern const double SRTarget1Trailing = 161; 
extern const int SRTarget2 = 2;
extern const double SRTarget2Trailing = 261;
extern const int SRTarget3 = 3;
extern const double SRTarget3Trailing = 361; 

/* SR Detection Settings */

extern const color WeeklySRColor = clrRed;
extern const color DailySRColor = clrBlue;

extern const int WeeklyTolerance = 1500;
extern const int DailyTolerance = 600;
extern const int Lookback = 500;
extern const int HitThresh = 15;

/* Trend Determination */
extern const bool CheckTrend = false;
extern const ENUM_APPLIED_PRICE ShortTermWeeklyMAAppliedPrice = PRICE_CLOSE;
extern const ENUM_MA_METHOD ShortTermWeeklyMAMethod = MODE_SMA; 
extern const int ShortTermWeeklyMAPeriod = 14; 

extern const ENUM_APPLIED_PRICE LongTermWeeklyMAAppliedPrice = PRICE_CLOSE;
extern const ENUM_MA_METHOD LongTermWeeklyMAMethod = MODE_SMA; 
extern const int LongTermWeeklyMAPeriod = 21; 
extern const int MonthlyTrendThreshold = 5000;

/* Used to simulate OnTimer firing in testing */
datetime lastOnTimerExecution;

bool hasDailyOrder = false; 
CSheldonOrder* dailyOrder = NULL;
   
ENUM_DIRECTION dailyDirection = DIRECTION_NONE;
ENUM_DIRECTION h4Direction = DIRECTION_NONE;

CChartContext* chartContext;
   
SRLevels *dailySRLevels;
SRLevels *weeklySRLevels;

PinBarTrader *dailyPinBarTrader;
PinBarTrader *h4PinBarTrader;

IBTrader *dailyIBTrader;
IBTrader *h4IBTrader;

CSheldonOrderManager *orderManager;

CiMA *MA1 = new CiMA();
CiMA *MA2 = new CiMA();

datetime lastH4;
datetime lastDaily; 

void EAAlert(string s) {
   Alert(APP_NAME, " v",APP_VERSION,": ",s);
}

void applyLocalTemplate() {
   CChart* chart = new CChart();
   chart.Attach();
   chart.ColorBackground(clrWhite);
   chart.ColorForeground(clrBlack);
   chart.ColorCandleBear(clrRed);
   chart.ColorCandleBull(clrGreen);
   chart.ColorGrid(clrLightGray);
   chart.ColorBarUp(clrBlack);
   chart.ColorBarDown(clrBlack);
   chart.Mode(CHART_CANDLES);
   
   delete chart;
}

int InitFail(string err_msg) {
   EAAlert(err_msg);
   return(INIT_FAILED);
}

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
   if (!IsConnected()) return InitFail(GetMsg(MSG_NOT_CONNECTED));
   if (!IsExpertEnabled()) return InitFail(GetMsg(MSG_EXPERT_NOT_ENABLED));
   if (!IsTradeAllowed()) return InitFail(GetMsg(MSG_TRADE_NOT_ALLOWED));
   bool res = EventSetTimer(TIMER_DELAY);
   if (IsTesting()) lastOnTimerExecution = TimeCurrent();
   
   // ChartApply template doesn't work in backtesting so we 
   // gotta simulate that with a hardcoded template
   if (IsTesting()) {
      applyLocalTemplate();
   }
      
   if (UseMA) {
      MA1.Create(Symbol(), PERIOD_D1, MA1Period, 0, MA1Method, MA1AppliedPrice);
      MA2.Create(Symbol(), PERIOD_D1, MA2Period, 0, MA2Method, MA2AppliedPrice);
   }
   
   CiMA* longTermWeeklyMA = new CiMA(); 
   longTermWeeklyMA.Create(
      Symbol(), 
      PERIOD_W1, 
      LongTermWeeklyMAPeriod, 
      0,
      LongTermWeeklyMAMethod,
      LongTermWeeklyMAAppliedPrice
   );
   
   CiMA* shortTermWeeklyMA = new CiMA(); 
   shortTermWeeklyMA.Create(
      Symbol(), 
      PERIOD_W1, 
      ShortTermWeeklyMAPeriod, 
      0,
      ShortTermWeeklyMAMethod,
      ShortTermWeeklyMAAppliedPrice
   );
   
   dailySRLevels = (new SRLevels())
      .setPeriod(PERIOD_D1)
      .setColour(DailySRColor)
      .setTolerance(DailyTolerance)
      .setLookback(Lookback)
      .setHitThresh(HitThresh)
      .setSRName(DAILY_SR_NAME);
   
   weeklySRLevels = (new SRLevels())
      .setPeriod(PERIOD_W1)
      .setColour(WeeklySRColor)
      .setTolerance(WeeklyTolerance)
      .setLookback(Lookback)
      .setHitThresh(HitThresh)
      .setSRName(WEEKLY_SR_NAME);
   
   
   chartContext = (new CChartContext())
      .setDailySRLevels(dailySRLevels)
      .setWeeklySRLevels(weeklySRLevels)
      .setShorterTermMA(shortTermWeeklyMA)
      .setLongerTermMA(longTermWeeklyMA)
      .setTrendThreshold(MonthlyTrendThreshold)
      .useTrendToggle()
      .load();
   
   orderManager = new CSheldonOrderManager();
   orderManager.setExponentialRisk(ExponentialRisk);
   orderManager.setStableRisk(StableRisk);
   orderManager.AcceptableRatio(AcceptableRatio);
   orderManager.setTradeStable(TradeStable);
   orderManager.Slippage(Slippage);
   
   s_sr_target t1; 
   s_sr_target t2; 
   s_sr_target t3; 
   t1.offset = SRTarget1;
   t1.trail_percent = SRTarget1Trailing;
   t2.offset = SRTarget2;
   t2.trail_percent = SRTarget2Trailing;
   t3.offset = SRTarget3;
   t3.trail_percent = SRTarget3Trailing;
   
   orderManager.addSRTarget(t1);
   orderManager.addSRTarget(t2);
   orderManager.addSRTarget(t3);
   
   // Set up the traders
   dailyIBTrader = new IBTrader();
   dailyIBTrader.setPeriod(PERIOD_D1);
   dailyIBTrader.setSLBufferPercent(DailySLBufferPercent);
   dailyIBTrader.setTakeProfitPercent(DailyIBTakeProfitPercent);
   dailyIBTrader.setEntryPercent(IBEntryPercent);
   dailyIBTrader.setExpirationHrs(DailyExpirationHrs);
   dailyIBTrader.setTradeBothSides(SetOnBothSides);
   dailyIBTrader.setTrendThreshold(DailyIBTrendThreshold);
   dailyIBTrader.setSlowdownLookback(SlowdownLookback);
   dailyIBTrader.setMAs(MA1, MA2);
   
   h4IBTrader = new IBTrader();
   h4IBTrader.setPeriod(PERIOD_H4);
   h4IBTrader.setSLBufferPercent(H4SLBufferPercent);
   h4IBTrader.setTakeProfitPercent(H4IBTakeProfitPercent);
   h4IBTrader.setEntryPercent(IBEntryPercent);
   h4IBTrader.setExpirationHrs(H4ExpirationHrs);
   h4IBTrader.setTradeBothSides(SetOnBothSides);
   h4IBTrader.setTrendThreshold(DailyIBTrendThreshold);
   h4IBTrader.setSlowdownLookback(SlowdownLookback);
   h4IBTrader.setMAs(MA1, MA2);
   
   dailyPinBarTrader = new PinBarTrader();
   dailyPinBarTrader.setPeriod(PERIOD_D1);
   dailyPinBarTrader.setSLBufferPercent(DailySLBufferPercent);
   dailyPinBarTrader.setTakeProfitPercent(DailyPinTakeProfitPercent);
   dailyPinBarTrader.setPinEntryPercent(PinEntryPercent);
   dailyPinBarTrader.setSlowdownThreshold(DailySlowdownThreshold);
   dailyPinBarTrader.setExpirationHrs(DailyExpirationHrs);
   dailyPinBarTrader.setSlowdownLookback(SlowdownLookback);
   dailyPinBarTrader.setPinBarPercent(PinBarPercent);
   dailyPinBarTrader.setRetracementLookback(PinRetracementLookback);
   dailyPinBarTrader.setRetracementLowerBound(DailyPinRetracementLowerBound);
   dailyPinBarTrader.setRetracementUpperBound(DailyPinRetracementUpperBound);
   
   h4PinBarTrader = new PinBarTrader();
   h4PinBarTrader.setPeriod(PERIOD_H4);
   h4PinBarTrader.setSLBufferPercent(H4SLBufferPercent);
   h4PinBarTrader.setTakeProfitPercent(H4PinTakeProfitPercent);
   h4PinBarTrader.setPinEntryPercent(PinEntryPercent);
   h4PinBarTrader.setSlowdownThreshold(H4SlowdownThreshold);
   h4PinBarTrader.setExpirationHrs(H4ExpirationHrs);
   h4PinBarTrader.setSlowdownLookback(SlowdownLookback);
   h4PinBarTrader.setPinBarPercent(PinBarPercent);
   dailyPinBarTrader.setRetracementLookback(PinRetracementLookback);
   dailyPinBarTrader.setRetracementLowerBound(H4PinRetracementLowerBound);
   dailyPinBarTrader.setRetracementUpperBound(H4PinRetracementUpperBound);
   
   CTrader* all_traders[4]; 
   all_traders[0] = dailyIBTrader;
   all_traders[1] = h4IBTrader; 
   all_traders[2] = dailyPinBarTrader; 
   all_traders[3] = h4PinBarTrader;
   
   // Set defaults for all traders 
   for (int i = 0; i < ArraySize(all_traders) ; i++) {
      all_traders[i]
         .setOrderManager(orderManager)
         .setChartContext(chartContext)
         .setCheckTrend(CheckTrend);
   }
   
   Alert("Initialize");
   return(INIT_SUCCEEDED);
  }
  
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
  
   // orderManager.writeOrders();
   delete orderManager;
   delete dailySRLevels;
   delete weeklySRLevels;
   delete MA1;
   delete MA2;
   delete dailyPinBarTrader;
   delete dailyIBTrader;
   delete h4PinBarTrader;
   delete h4IBTrader;
   delete chartContext;
   delete dailyOrder;
   EventKillTimer();
  }

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
{
   /* Use this if your EA saves trailing stops in the trade comments */
   orderManager.UpdateActiveTrails(); 
}
//+------------------------------------------------------------------+

void OnChartEvent(const int id,         // Event ID
                  const long& lparam,   // Parameter of type long event
                  const double& dparam, // Parameter of type double event
                  const string& sparam  // Parameter of type string events
  ) {
  chartContext.OnEvent(id, lparam, dparam, sparam);
  
  }

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   /* OnTimer event does not fire in the backtester so we have to test it in this hacky way */
   if (IsTesting() &&  TimeCurrent() > lastOnTimerExecution + TIMER_DELAY){
      OnTimer();
      lastOnTimerExecution = TimeCurrent();
   }
   
   chartContext.OnTick();
   datetime currH4, currDaily;
   
   currH4 = iTime(Symbol(), PERIOD_H4, 0);
   if(lastH4 != currH4){
      lastH4 = currH4;
   } else {
      return;
   }
   
   double weekly_sr[], daily_sr[];
   chartContext.refresh();
   CSheldonOrder* h4Order;
   if (TradeH4) {
      h4Order = new CSheldonOrder();
      
      // trade the lower timeframe if there is high momentum on the daily
      if(chartContext.shouldTradeLowerTimeframe(PERIOD_D1)) {
         ENUM_NO_TRADE_REASON no_trade_reason = 
            h4IBTrader.execute(h4Order);
         if (no_trade_reason == NO_TRADE_REASON_NO_SIGNAL) {
            no_trade_reason = h4PinBarTrader.execute(h4Order);
         }
      // if there is no high momentum on the daily, trade lower timeframe if there is a daily order confirmation
      } else if(hasDailyOrder) {
         ENUM_NO_TRADE_REASON no_trade_reason = h4IBTrader.executeWithGoverningOrder(h4Order, dailyOrder);
         if (no_trade_reason == NO_TRADE_REASON_NO_SIGNAL) {
            no_trade_reason = h4PinBarTrader.executeWithGoverningOrder(h4Order, dailyOrder);
         }
      }
      delete h4Order;
   }
   
   currDaily = iTime(Symbol(), PERIOD_D1, 0);
   if(lastDaily != currDaily){
      lastDaily = currDaily;
   } else {
      return;
   }
   
   delete dailyOrder;
   dailyOrder = new CSheldonOrder();
   hasDailyOrder = false;
   ENUM_NO_TRADE_REASON no_trade_reason = dailyIBTrader.execute(dailyOrder);
   if (no_trade_reason == NO_TRADE_REASON_NO_SIGNAL) {
      no_trade_reason = dailyPinBarTrader.execute(dailyOrder);
   }
   
   if (no_trade_reason == NO_TRADE_REASON_NONE) {
      hasDailyOrder = true;
   } 
}