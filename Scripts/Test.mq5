//+------------------------------------------------------------------+
//|                                                         Test.mq5 |
//|                        Copyright 2020, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2020, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

#define POST_URI "https://maker.ifttt.com/trigger/sheldon_notif/with/key/cU1eJuq7DJSDqjkVs9rxxh"

//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+

void sendMessage(string messageText) {
  string queryParams = "&";
  char data[];
  
  string symbolStr = Symbol();
  queryParams += "value1="+symbolStr;
  queryParams += "&value2="+messageText;
  
  StringToCharArray(queryParams, data);
  char result[];
  string result_headers;
  int timeout = 0;
  // send phone call when trade is sent
  int response = WebRequest("POST", POST_URI, "", timeout, data, result, result_headers);
  Alert(response);
}

void OnStart()
  {
  
  sendMessage("Flow detected");

     
  }
//+------------------------------------------------------------------+
