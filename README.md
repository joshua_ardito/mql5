# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Sheldon. Forex Automated Trading Robot###

* Quick summary
* Version 1.0

### How do I get set up? ###

* Merge the contents of this MQL4 folder to your local MQL4 folder.
* Open MQL4 and under "Expert Advisors", load "Sheldon"

## SRFinder.mq4 ##
Script used to detect support and resistance levels on the current chart using the following algorithm. (Words in parentheses are inputs to the algorithm)

* Searches for bars that are local maximums/minimums in the last [lookback] bars. 
This means that the bars immediately before and after that bar either both have higher highs or both have lower lows, indicating that the bar in question is at a turning point of the price.

* Checks the highs of the local maximums and the lows of the local minimums, those within [tolerance] pips from each other are considered to be on the same SR.

* Each point is now associated with a specific SR level, any level with at least [HitThresh] points on it, is considered a relevant SR.

* The exact value of the SR level is found by taking an average of the points associated with it.


## How it Works ##

Sheldon trades two main signals, on two main time frames. Pin Bars and Inside Bars on the H4 and the Daily. 
It risks a percentage of the account balance on each signal. Depending on the criteria met, it will trade using [StableRisk] or [ExponentialRisk].

## Pin Bars ##
When does it trade them? 

Pin Bar signals are traded when they are considered to appear after slow market momentum ^[1].
If the body of a pin bar crosses a weekly SR level, it will be traded with exponential Risk

How is the direction determined? By the direction in which the pin bar is pointing (daah). 

## Inside Bars ##
When does it trade them? 

Inside Bars are always traded with at least stable risk.
If the market momentum is greater than [IBTrendThreshold] pips, it is traded with exponential risk.

How is the direction determined? 

If [SetOnBothSides] is set to true...well...we set both a buy and a sell.

If [UseMA] is set to true, we use a moving average method to determine the direction of the trade. 
If the value of MA1 is greater than the value of MA2, a buy order is set. Otherwise, a sell order is set.

Otherwise, we fall back to the default behaviour which is to measure the momentum^[1] of the market. If it's upwards, we buy, otherwise we sell.

## Take Profits  (Outdated) ##

Take profits are based upon one fib level, and up to 3 SR targets. The SRTarget params take an integer repsenting the number of SR's in the 
profit direction that you want to set a TP at. 
* TakeProfitPercent - One tp by default is always determined as this percentage of the size of the bar of the signal. 
	If no SR's can be detected in the profit direction of the trade, this is the only tp that will be set.
* SRTarget1 - The number of SR's above/below entry for the first tp. 
* SRTarget2 - The number of SR's above/below entry for the second tp. 
* SRTarget3 - The number of SR's above/below entry for the third tp. 

* If you want less than 3 SRTargets, set some to 0. 
* Any trades below the AcceptableRatio will not be considered
* Risk is split evenly across all considered trades. 

This means if I have TakeProfitPercent = 261, SRTarget1 = 1, SRTarget2 = 2, SRTarget3 = 0, and
the signal is exponential, AND the first SR level is too close, and under the AcceptableRatio, then only 2 TP's will be set: 
SRTarget2 and TakeProfitPercent. Assume the signal was exponential, the total percentage risk for each trade would be ExponentialRisk / 2. 

## H4 Signals ##

H4 signals are traded with the same criteria as Daily signals. The only caveat is that a trade must have been made on the current daily candle
in order to consider trading any h4  signals. Many parameters have their own set values for the H4, which are denoted by "H4" in front of the name. 
Similarly, parameters specific to the daily timeframe will have "Daily" in front of their name.

#### Measuring Momentum [1]###

* Pin Bar signals are traded when they are considered to appear after slow market momentum.
* Inside Bar signals are traded exponentially when they are considered to appear after high market momentum. 

* We say the momentum is slow if the total amount of pips that the market has moved up or down, for the last [SlowdownLookback] bars, 
* is less than the [SlowdownThreshold]. 

* Note the difference between momentum and volatility. If a candle opened at 1.20000 and 
* the candle 4 candles after that closed at 1.20000 but between that time it rose 400 pips and dropped back down 400 pips, the measured
* momentum here is still 0.

## General Properties ##

* PinBarPercent - The minimum percent of the candle that should be tail-end wick, to be considered a pin bar.
* TradeH4 - Whether or not to set orders based on the h4 timeframe. 

## Slowdown Properties ## 
* SlowdownLookback - The amount of previous candles to consider in the measurement of market slowdown. 

* DailySlowdownThreshold - The amount of pips over which, the market will NOT be considered to be in a slowdown on the daily. 
* H4SlowdownThreshold - As above, for H4. 

* DailyIBTrendThreshold - If the market has moved in the buy direction more than this amount of pips, 
  it will be considered to be trending upwards, if that matches with the IB trade direction, it will be exponential. 
  Same for the sell direction. 


* H4IBTrendThreshold - As above, for H4. 

## Money Management Properties ##
* ExponentialRisk - The percentage of the account balance that should be risked on an exponential trade.
* StableRisk - The percentage of the account balance that should be risked on a stable trade.
* TradeStable - Determines whether or not to take trades determined to be stable risk. 
* AcceptableRatio - The minimum reward/risk ratio that the EA will be willing to set a trade on.

## IB Direction Determination Properties ##

* SetOnBothSides - When set, this will set orders on both sides of any eligible inside bar signal. (beta mode, might not work)

The following properties are used to configure 2 moving averages which will determine whether an IB signal is a BUY or a SELL. 
If MA1 > MA2 then it's a buy, otherwise it's a sell. NOTE: If TradeBothSides is set to true, these moving averages will be ignored. 

* UseMA - Determines whether we use this MA method for determining the direction of an IB trade. If set to false, 
          we will default to using the trend threshold. 
          
Properties for MA1:
* MA1AppliedPrice 
* MA1Method 
* MA1Period

Properties for MA2:
* MA2AppliedPrice
* MA2Method
* MA2Period

## Pin Retracement Properties ## 

* PinRetracementLookback - The amount of candles to use for determining whether or not the market is in retracement. 

* DailyPinRetracementLowerBound - The amount of pips above below which the market will not be considered to be in retracement. 
* H4PinRetracementLowerBound - As above for h4 timeframe. 

* DailyPinRetracementUpperBound - The amount of pips above which the market will not be considered to be in retracement. 
* H4PinRetracementUpperBound - As above for h4 timeframe. 


## General Order Stops Settings ##
* PinEntryPercent - The percentage of the pin bar at which to set the entry.
* IBEntryPercent - The percentage of the IB at which to set the entry. 

* DailySLBufferPercent - How far away from the end of the candle should the SL be set, as a percentage of the candle.
* H4SLBufferPercent - As above for h4 candles.

* DailyIBTakeProfitPercent - Where to set the take profit for daily IB orders, as a fib level of the daily candle. 
* H4IBTakeProfitPercent - As above for h4 IB. 
* DailyPinTakeProfitPercent - As above for Daily Pin. 
* H4PinTakeProfitPercent - As above for h4 pin. 

## SR Target Settings ## 

* SRTarget1 - The amount of SR above entry price to set tp . 
* SRTarget1Trailing - The trailing stop of the the order with TP of SRTarget1, as a percentage of the signal candle. 
... 

## Order Metadata Settings ## 
* Slippage - The amount of pips to set as the slippage for each trade. 
* DailyExpirationHrs - The amount of hours that a pending daily trade will stay open before expiring.  
* H4ExpirationHrs - As above for H4 timeframe

## SR Detection Properties ## 
* WeeklySRColor - The color of the horizontal lines displayed on the chart for Weekly SR levels. 
* DailySRColor - The color of the horizontal lines displayed on the chart for Daily SR levels. 
* WeeklyTolerance - The maximum difference between market reversal points which would be considered on the same SR level. 
* DailyTolerance - As above for the daily timeframe. 
* Lookback - The amount of previous candles to use in the SR calculation. 
* HitThresh - The amount of reversal points that must be on an SR level for it to be considered valid. 

## Trend Determination Properties ## 

* CheckTrend - Whether or not to check the trend to determine whether or not a trade should be exponential. 

Properties for the short term weekly MA: 
* ShortTermWeeklyMAAppliedPrice 
* ShortTermWeeklyMAMethod 
* ShortTermWeeklyMAPeriod 

Properties for the long term weekly MA: 
* LongTermWeeklyMAAppliedPrice 
* LongTermWeeklyMAMethod
* LongTermWeeklyMAPeriod 

* Monthly Threshold - If the long term MA is greater than the short term MA by this amount of pips, the market is trending downward. 
* If it's less than the short term MA by this amount of pips, the market is trending upward. If it's neither, then the market is in sideways
* movement. 

## Gotchas (Outdated) ##

There is a lot of criteria working together here, and sometimes they can work against each other. Here are some important weird cases to note. 

* If some of your SR target levels would cause the trade's risk/reward ratio to be less than your AcceptableRatio, Sheldon will not set those, 
	 but still distribute your risk evenly across the other levels that have acceptable ratios. It should probably not set any trades if any of 
	 the targets are unacceptable. 
 
 * If your fib level trade would result in a lower risk/reward ratio than your AcceptableRatio, it will pretty much get ignored every time. 
	 We should probably not consider risk/reward when setting fib level trades. If you choose to force a slappy risk/reward that's your problem bro.
 
 * Nothing is stopping us from having open buys and sells at the same time on the daily. If we trade a daily up pin, and before it hits TP,
	 a daily down pin is formed, we could trade both. If we run into the case where a signal comes up in the opposite direction of a trade we currently
	 have open, we can either cancel the old trade in favour of the new, prevent Sheldon from opening the new one, in favour of the old, or 
	 determine based on probability the better trade, and leave only that one open. 
 
 * We only set on the h4 if we executed a trade on the most recent bar. If a daily signal lasts multiple days, and there are no further daily
 	signals in the days past day 1, we won't look for h4 scale-ups after day 1. We should probably look for h4 trades as long as a daily trade
	is either pending or active. Or maybe a criteria should be that the signal entry is in the profit zone of the daily trade? 
	
 
 
 